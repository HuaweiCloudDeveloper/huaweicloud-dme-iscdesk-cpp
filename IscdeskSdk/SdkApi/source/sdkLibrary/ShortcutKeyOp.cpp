/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#include <mutex>
#include <shared_mutex>
#include <string>
#include "../../include/sdkLibrary/DataType.h"
#include "../../include/log/Loger.h"
#include "../../include/sdkLibrary/StringConvert.h"
#include "../../include/sdkLibrary/ShortcutKeyOp.h"
#include "../../include/sdkLibrary/ProcessOp.h"

namespace SDK
{
	std::map<std::string, std::vector<short>>	g_shortcutKey;		//需要监听的快捷键
	HookFuncPtr									g_shortcutKeyProc;	//快捷键处理回调函数
	std::shared_mutex							g_shortcutKeyMutex;	//快捷键配置读写锁

	LRESULT CALLBACK ShortcutKeyMonitor(_In_ int nCode, _In_ WPARAM wParam, _In_ LPARAM lParam)
	{
		KBDLLHOOKSTRUCT* ks = reinterpret_cast<KBDLLHOOKSTRUCT*>(lParam);		// 包含低级键盘输入事件信息

		/* 获取前景窗口所属进程以及当前进程的id */
		DWORD fgProcId;
		HWND hwnd = GetForegroundWindow();
		GetWindowThreadProcessId(hwnd, &fgProcId);

		/* 判断如果前景窗口所属进程非当前进程，则不处理 */
		if (g_processId != fgProcId)
		{
			return CallNextHookEx(NULL, nCode, wParam, lParam);
		}

		/* 当前按键为按下的状态（ctrl和shift按下时flags为0，alt按下时flags为32） */
		if (ks->flags == 0 || ks->flags == 32)
		{
			/* 检测Ctrl Alt Shift三个辅助按键是否按下 */
			bool ctrlKey = ((uint16)GetKeyState(VK_LCONTROL) & 0x8000) || ((uint16)GetKeyState(VK_RCONTROL) & 0x8000);
			bool altKey = ((uint16)GetKeyState(VK_LMENU) & 0x8000) || ((uint16)GetKeyState(VK_RMENU) & 0x8000);
			bool shiftKey = ((uint16)GetKeyState(VK_LSHIFT) & 0x8000) || ((uint16)GetKeyState(VK_RSHIFT) & 0x8000);

			/* 加锁 */
			std::shared_lock<std::shared_mutex> lock(g_shortcutKeyMutex);

			for (const auto& its : g_shortcutKey)
			{
				bool firstKey = ctrlKey ^ (its.second[0] == -1);
				bool secondKey = altKey ^ (its.second[1] == -1);
				bool thirdKey = shiftKey ^ (its.second[2] == -1);

				/* 当快捷键都按下时，生成快捷键事件 */
				if (firstKey && secondKey && thirdKey && ks->vkCode == (DWORD)its.second[3])
				{
					g_shortcutKeyProc(its.first);
					return true;
				}
			}
		}

		// 将消息传递给钩子链中的下一个钩子
		return CallNextHookEx(NULL, nCode, wParam, lParam);
	}

	ShortcutKeyOp::ShortcutKeyOp() :m_hook(NULL) {
	}

	int ShortcutKeyOp::RegisterWindowsHook(HookFuncPtr func)
	{
		if (m_hook != NULL)
		{
			UnRegisterWindowsHook();
		}

		m_hook = SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC)ShortcutKeyMonitor, GetModuleHandleA(NULL), NULL);
		if (m_hook == NULL)
		{
			GLogInfo("ShortcutKeyOp install hook failed\n");
			return -1;
		}

		GLogInfo("ShortcutKeyOp install hook success\n");

		g_shortcutKeyProc = func;

		return 0;
	}

	void ShortcutKeyOp::UnRegisterWindowsHook()
	{
		if (m_hook != NULL)
		{
			UnhookWindowsHookEx(m_hook);
			m_hook = NULL;

			GLogInfo("ShortcutKeyOp uninstall hook\n");
		}
	}

	void ShortcutKeyOp::ShortcutKeyConfigParse(std::string msg)
	{
		std::unique_lock<std::shared_mutex> lock(g_shortcutKeyMutex);

		/* 快捷键配置修改时，先清空原快捷键,因为快捷键可能会关闭 */
		g_shortcutKey.clear();

		std::vector<std::string> shortcutKeyNames = {
			"ShortcutOpenCloud",
			"ShortcutSaveToCloud", 
			"ShortcutSaveAsToCloud" 
		};

		/* 解析快捷键配置 */
		for (size_t i = 0; i < shortcutKeyNames.size(); i++)
		{
			std::string str = StringConvert::ExtractStringBetween(msg, shortcutKeyNames[i] + "\":{", "}");
			if (str.empty())
			{
				continue;
			}

			std::string accelerator = StringConvert::ExtractStringBetween(str, "accelerator\":\"", "\"");
			std::string enable;
			size_t index = str.find("enable\":");
			if (index != std::string::npos)
			{
				enable = str.substr(index + strlen("enable\":"));
			}

			if (accelerator.empty() || enable.empty() || enable == "false")
			{
				continue;
			}

			g_shortcutKey[shortcutKeyNames[i]] = ShortcutKeyAarrange(accelerator);
		}
	}

	std::vector<short> ShortcutKeyOp::ShortcutKeyAarrange(std::string accelerator)
	{
		std::vector<short> shortcutKey(4, -1);

		accelerator += "+";
		std::vector<short> keys{ VK_CONTROL, VK_MENU, VK_SHIFT };
		size_t size = keys.size();
		size_t index = accelerator.find("+");
		while (index != std::string::npos)
		{
			std::string key = accelerator.substr(0, index);
			
			short value;
			try
			{
				value = static_cast<short>(stoi(key));
			}
			catch (const std::invalid_argument& e)
			{
				GLogError("error accelerator key :%s\n", e.what());
				continue;
			}
			catch (const std::out_of_range& e)
			{
				GLogError("error accelerator key :%s\n", e.what());
				continue;
			}

			for (size_t count = 0; count < size; count++)
			{
				if (keys[count] == value)
				{
					shortcutKey[count] = value;
					break;
				}

				if (count == size - 1 && !key.empty())
				{
					shortcutKey[size] = value;
				}
			}

			accelerator = accelerator.substr(index + 1);
			index = accelerator.find("+");
		}

		return shortcutKey;
	}
}
