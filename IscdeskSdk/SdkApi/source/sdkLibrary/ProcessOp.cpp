/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#include <windows.h>
#include <process.h>
#include <tlhelp32.h>
#include "../../include/sdkLibrary/Defines.h"
#include "../../include/log/loger.h"
#include "../../include/sdkLibrary/StringConvert.h"
#include "../../include/sdkLibrary/ProcessOp.h"

namespace SDK
{
	DWORD g_processId = GetCurrentProcessId();	//当前进程id
	std::vector<HWND> ProcessOp::m_windowHandles{};

	int ProcessOp::GetIscdeskClientInstallPath(std::string& path)
	{
		/* 获取iscdesk客户端安装路径 */
		HKEY hKey, hKey1;
		LPCTSTR data_Set;							//主键值
		DWORD dataSize = 2 * MAX_PATH_LEN;			//数据长度
		char data[2 * MAX_PATH_LEN] = "";

		/* 打开kooapp键 */
		data_Set = KOOAPP_REGISTRY_KEY_1;
		if (RegOpenKeyEx(HKEY_CLASSES_ROOT, data_Set, NULL, KEY_READ, &hKey) != ERROR_SUCCESS)
		{
			GLogDebug("open key [%s] in registry failed\n", KOOAPP_REGISTRY_KEY_1);
			return -1;
		}

		/* 打开shell键 */
		data_Set = KOOAPP_REGISTRY_KEY_2;
		if (RegOpenKeyEx(hKey, data_Set, NULL, KEY_READ, &hKey1) != ERROR_SUCCESS)
		{
			GLogDebug("open key [%s] in registry failed\n", KOOAPP_REGISTRY_KEY_2);
			return -1;
		}

		/* 打开open键 */
		data_Set = KOOAPP_REGISTRY_KEY_3;
		if (RegOpenKeyEx(hKey1, data_Set, NULL, KEY_READ, &hKey) != ERROR_SUCCESS)
		{
			GLogDebug("open key [%s] in registry failed\n", KOOAPP_REGISTRY_KEY_3);
			return -1;
		}

		/* 打开command键 */
		data_Set = KOOAPP_REGISTRY_KEY_4;
		if (RegOpenKeyEx(hKey, data_Set, NULL, KEY_READ, &hKey1) != ERROR_SUCCESS)
		{
			GLogDebug("open key [%s] in registry failed\n", KOOAPP_REGISTRY_KEY_4);
			return -1;
		}

		/* 查询""中的值 */
		if (RegQueryValueEx(hKey1, KOOAPP_REGISTRY_KEY_5, NULL, NULL, (LPBYTE)data, &dataSize) != ERROR_SUCCESS)
		{
			GLogDebug("query value [%s] in registry failed\n", KOOAPP_REGISTRY_KEY_5);
			return -1;
		}

		/* 从注册表解析iscdesk的安装路径 */
		path = StringConvert::ExtractStringBetween(data, "\"", "\"");
		if (path.empty())
		{
			GLogInfo("Iscdesk Client is not Install\n");
			return -1;
		}

		return 0;
	}

	int ProcessOp::CheckIscdeskClientExecute()
	{
		/* 获取iscdesk安装路径 */
		std::string iscdeskPath;
		if (GetIscdeskClientInstallPath(iscdeskPath) < 0)
		{
			return -1;
		}

		/* 截取当前系统进程快照 */
		HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		if (hSnapshot == INVALID_HANDLE_VALUE)
		{
			GLogError("get process snapshot failed\n");
			return -1;
		}

		PROCESSENTRY32 pe32;
		pe32.dwSize = sizeof(PROCESSENTRY32);

		/* 查找iscdesk客户端是否启动 */
		if (!Process32First(hSnapshot, &pe32))
		{
			CloseHandle(hSnapshot);
			return -1;
		}

		do
		{
			/* 获取此进程完整路径 */
			HANDLE process = OpenProcess(PROCESS_QUERY_INFORMATION, false, pe32.th32ProcessID);
			if (process == NULL)
			{
				continue;
			}

			char path[MAX_PATH_LEN];
			int len = MAX_PATH_LEN;
			if (!QueryFullProcessImageName(process, 0, path, (PDWORD)&len))
			{
				CloseHandle(process);
				continue;
			}
			CloseHandle(process);

			/* 判断此进程是否为iscdesk客户端 */
			if (iscdeskPath.compare((const char*)path) == 0)
			{
				CloseHandle(hSnapshot);
				GLogInfo("Iscdesk client is running\n");
				return 0;
			}
		} while (Process32Next(hSnapshot, &pe32));

		CloseHandle(hSnapshot);
		return -1;
	}

	void ProcessOp::SetForegroundMainWindow()
	{
		/* 如果前景窗口是当前进程，则无需额外处理 */
		HWND hwnd = GetForegroundWindow();

		DWORD processId;
		GetWindowThreadProcessId(hwnd, &processId);
		if (processId == g_processId)
		{
			return;
		}

		m_windowHandles.clear();
		EnumWindows(EnumWindowsProc, NULL);

		for (auto windowHandle : m_windowHandles)
		{
			if (!IsIconic(windowHandle))
			{
				/* 如果不是最小化窗口，则把窗口置为最顶部，且不改变窗口大小 */
				SetWindowPos(windowHandle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
				SetWindowPos(windowHandle, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
				GLogInfo("SDK Set Window to Forward\n");			}
			else
			{
				/* 如果窗口已最小化，则将窗口还原 */
				ShowWindow(windowHandle, SW_RESTORE);
				GLogInfo("SDK Restore Minimized Window\n");
			}

			/* 置为前景窗口 */
			SetForegroundWindow(windowHandle);
		}
	}
}

/* private函数 */
namespace SDK
{
	BOOL ProcessOp::EnumWindowsProc(HWND hWnd, LPARAM lParam)
	{
		/* 判断是否主窗口并且可见 */
		if ((GetParent(hWnd) == NULL) && IsWindowVisible(hWnd))
		{
			DWORD windProcessId;
			GetWindowThreadProcessId(hWnd, &windProcessId);

			if (windProcessId == g_processId)
			{
				/* 保存窗口句柄 */
				m_windowHandles.push_back(hWnd);

				return TRUE;
			}
		}

		return TRUE;
	}
}