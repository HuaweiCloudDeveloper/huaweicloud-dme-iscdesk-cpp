/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#include <time.h>
#include <string>
#include "../../include/sdkLibrary/TimeClock.h"

namespace SDK
{
	uint64 GetInitTimeNs();

	uint64 TimeClock::m_freq = TimeClock::GetPerformanceFreq();
	uint64 TimeClock::m_initclk = TimeClock::GetPerformanceCounter();
	uint64 TimeClock::m_initns = GetInitTimeNs();
	const int INT_1E9 = 1000000000;
	const int INT_1E3 = 1000;
	const int DATE_STRING_LEN = 1024;

	uint64 GetInitTimeNs()
	{ 
		time_t cutTime = time(NULL);

		SYSTEMTIME localTime;
		GetLocalTime(&localTime);

		return cutTime * INT_1E9 + localTime.wMilliseconds * 1000000;
	}

	SDK::uint64 TimeClock::GetCurrentTimeNs()
	{
		uint64 cur = GetPerformanceCounter();

		return (cur - m_initclk) * (INT_1E9 / m_freq) + m_initns;
	}

	SDK::uint64 TimeClock::GetPerformanceFreq()
	{
#ifdef _WIN32
		LARGE_INTEGER freq;
		QueryPerformanceFrequency(&freq);
		return freq.QuadPart;
#endif
	}

	SDK::uint64 TimeClock::GetPerformanceCounter()
	{
#ifdef _WIN32
		LARGE_INTEGER tm;
		QueryPerformanceCounter(&tm);
		return tm.QuadPart;
#endif
	}
}

namespace SDK
{
	Date::Date(): m_year(0), m_month(0), m_day(0), m_hour(0),
		m_min(0), m_sec(0), m_msec(0), m_usec(0), m_nsec(0)
	{
		uint64 cur = TimeClock::GetCurrentTimeNs();
		ComputeCurrentDate(cur);
	}

	Date::Date(uint64 ns): m_year(0), m_month(0), m_day(0), m_hour(0),
		m_min(0), m_sec(0), m_msec(0), m_usec(0), m_nsec(0)
	{
		ComputeCurrentDate(ns);
	}

	std::string Date::GetCurrentDate()
	{
		char buff[DATE_STRING_LEN];
		if (sprintf_s(buff, DATE_STRING_LEN, "%04u-%02u-%02u %02u:%02u:%02u.%03u",
			m_year, m_month, m_day, m_hour, m_min, m_sec, m_msec) < 0)
		{
			return "";
		}

		return std::string(buff);
	}

	void Date::ComputeCurrentDate(uint64 ns)
	{
		/* 计算秒以下的时间 */
		m_nsec = ns % INT_1E3;
		ns = ns / INT_1E3;

		m_usec = ns % INT_1E3;
		ns = ns / INT_1E3;

		m_msec = ns % INT_1E3;
		ns = ns / INT_1E3;

		/* 根据当前秒数计算年月日时分秒 */
		time_t cutTime = (time_t)ns;
		struct tm* ltm = localtime(&cutTime);//此函数会自动修正时区，因此不需要自行修正时区
		if (ltm != NULL)
		{
			m_year = (uint32)ltm->tm_year + 1900;//tm_year是从1900开始的年数
			m_month = (uint32)ltm->tm_mon + 1;//tm_mon是从0~11
			m_day = (uint32)ltm->tm_mday;

			m_hour = (uint32)ltm->tm_hour;
			m_min = (uint32)ltm->tm_min;
			m_sec = (uint32)ltm->tm_sec;
		}
	}

	SDK::int8 Date::GetTimeZone()
	{
		TIME_ZONE_INFORMATION info;
		GetTimeZoneInformation(&info);

		return static_cast<int8>(info.Bias / -60);
	}

}
