/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <Windows.h>
#include <vector>
#include "../../include/sdkLibrary/StringConvert.h"

namespace SDK
{
	std::string StringConvert::ExtractStringBetween(const std::string& src, const std::string& left, 
		const std::string& right)
	{
 		size_t startIndex;
		size_t endIndex;
		GetStringPosition(src, left, right, startIndex, endIndex);

		if (startIndex != std::string::npos && endIndex > startIndex)
		{
			return src.substr(startIndex, endIndex - startIndex);
		}

		return "";
	}

	std::string StringConvert::ReplaceStringBetween(const std::string& src, const std::string& value, 
		const std::string& left, const std::string& right)
	{
		std::string ret = src;
		size_t startIndex;
		size_t endIndex;
		GetStringPosition(src, left, right, startIndex, endIndex);

		if (startIndex != std::string::npos && endIndex > startIndex)
		{
			return ret.replace(startIndex, endIndex - startIndex, value.c_str(), value.size());
		}

		return ret;
	}

	int StringConvert::StringToInt(const std::string& str)
	{
		int ret = 0;
		int symbol = 1;

		for (size_t i = 0; i < str.size(); i++)
		{
			if (str[i] >= '0' && str[i] <= '9')
			{
				ret *= 10;
				ret += str[i] - '0';
			}
			else if (i == 0 && str[i] == '-')
			{
				symbol = -1;
			}
			else if (i == 0 && str[i] == '+')
			{
				symbol = 1;
			}
			else
			{
				break;
			}
		}

		return ret * symbol;
	}

	std::string StringConvert::PathDoubleSlashesToSingle(const std::string& str, bool bReverse)
	{
		std::string ret = str;
		if (bReverse == false)
		{
			size_t index = ret.find("\\\\");
			while (index != std::string::npos)
			{
				ret = ret.replace(index, 2, "\\", 0, 1);
				index = ret.find("\\\\");
			}
		}
		else
		{
			size_t index = ret.find("\\");
			while (index != std::string::npos)
			{
				ret = ret.replace(index, 1, "\\\\", 0, 2);
				index = ret.find("\\", index + 2);
			}
		}

		return ret;
	}

	std::string StringConvert::PathDoubleSlashesToOpposite(const std::string& str, bool bReverse)
	{
		std::string ret = str;
		if (bReverse == false)
		{
			size_t index = ret.find("\\\\");
			while (index != std::string::npos)
			{
				ret = ret.replace(index, 2, "/", 0, 1);
				index = ret.find("\\\\");
			}
		}
		else
		{
			size_t index = ret.find("/");
			while (index != std::string::npos)
			{
				ret = ret.replace(index, 1, "\\\\", 0, 2);
				index = ret.find("/");
			}
		}

		return ret;
	}

	std::string StringConvert::PathSingleSlashesToOpposite(const std::string& str, bool bReverse)
	{
		std::string ret = str;
		if (bReverse == false)
		{
			size_t index = ret.find("\\");
			while (index != std::string::npos)
			{
				ret = ret.replace(index, 1, "/", 0, 1);
				index = ret.find("\\");
			}
		}
		else
		{
			size_t index = ret.find("/");
			while (index != std::string::npos)
			{
				ret = ret.replace(index, 1, "\\", 0, 1);
				index = ret.find("/");
			}
		}

		return ret;
	}

	std::string StringConvert::BoolToStdString(bool src)
	{
		if (src)
		{
			return "true";
		}
		else
		{
			return "false";
		}
	}

	std::string StringConvert::StringToUpper(const std::string& str)
	{
		std::string ret = str;

		for (size_t i = 0; i < str.size(); i++)
		{
			ret[i] = toupper(str[i]);
		}

		return ret;
	}

	std::string StringConvert::RemoveLastLevel(const std::string& str)
	{
		int index1 = (int)str.find_last_of('\\');
		int index2 = (int)str.find_last_of('/');

		int index = max(index1, index2);
		if (index == std::string::npos)
		{
			return str;
		}

		return str.substr(0, index);
	}

	std::string StringConvert::CanonicalizePath(std::string path, std::string delimiter)
	{
		std::vector<std::string> paths;

		while (true)
		{
			size_t index = FindSlashFromString(path);
			if (index == std::string::npos)
			{
				break;
			}

			std::string dir = path.substr(0, index);
			path = path.substr(index + 1);

			if (dir.empty() || dir == ".")
			{
				continue;
			}

			if (dir == "..")
			{
				if (!paths.empty())
				{
					paths.pop_back();
				}
			}
			else
			{
				paths.push_back(dir);
			}
		}

		if (!path.empty())
		{
			paths.push_back(path);
		}

		std::string canonicalizationPath;
		for (size_t i = 0; i < paths.size(); i++)
		{
			canonicalizationPath.append(paths[i]);
			if (i < paths.size() - 1)
			{
				canonicalizationPath.append(delimiter);
			}
		}

		return canonicalizationPath;
	}

	void StringConvert::GetStringPosition(const std::string& src, const std::string& left, 
		const std::string& right, size_t& startPos, size_t& endPos)
	{
		startPos = -1;
		endPos = -1;

		size_t startIndex = src.find(left);
		if (startIndex == std::string::npos)
		{
			return;
		}
		startIndex += left.size();

		size_t endIndex = src.find(right, startIndex);
		if (endIndex == std::string::npos)
		{
			return;
		}

		startPos = startIndex;
		endPos = endIndex;
	}

	size_t StringConvert::FindSlashFromString(const std::string& str)
	{
		size_t index;
		size_t index1 = str.find("/");
		size_t index2 = str.find("\\");
		if (index1 == std::string::npos)
		{
			if (index2 == std::string::npos)
			{
				return std::string::npos;
			}

			index = index2;
		}
		else
		{
			if (index2 == std::string::npos)
			{
				index = index1;
			}

			index = min(index1, index2);
		}

		return index;
	}
}