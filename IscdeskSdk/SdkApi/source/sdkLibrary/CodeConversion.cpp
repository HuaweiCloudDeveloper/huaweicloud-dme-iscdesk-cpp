/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#include <windows.h>
#include <string>
#include <bitset>
#include "../../include/sdkLibrary/DataType.h"
#include "../../include/sdkLibrary/CodeConversion.h"

namespace SDK
{
	std::string g_utf8Bom = "\xEF\xBB\xBF";
	std::string g_utf16LEBom = "\xFF\xFE";
	std::string g_utf16BEBom = "\xFE\xFF";

	typedef int(*ConvertorFunc)(const std::string& src, std::string& dst, EnCodeType srcType);
	typedef struct ConvertorFuncTable
	{
		EnCodeType	codeType;
		ConvertorFunc func;
	}ConvertorFuncTable;

	int CodeConvertor::CodeTypeConvert(const std::string& src, std::string& dst, EnCodeType dstType)
	{
		EnCodeType srctype = CodeConvert::CheckCodeType(src);
		int ret = 0;

		ConvertorFuncTable table[] = {
				{ANSI,		CodeConvertor::CodeConvertToANSI},
				{UTF8,		CodeConvertor::CodeConvertToUTF8},
				{UTF8_BOM,	CodeConvertor::CodeConvertToUTF8BOM},
				{UTF16_LE,	CodeConvertor::CodeConvertToUTF16LE},
				{UTF16_BE,	CodeConvertor::CodeConvertToUTF16BE}
		};

		for (int i = 0; i < sizeof(table) / sizeof(table[0]); i++)
		{
			if (dstType == table[i].codeType)
			{
				ret = table[i].func(src, dst, srctype);
			}
		}

		return ret;
	}

	int CodeConvertor::CodeConvertToANSI(const std::string& src, std::string& dst, EnCodeType srcType)
	{
		int ret = 0;
		switch (srcType)
		{
		case SDK::ANSI:
			ret = static_cast<int>(src.size());
			dst = src;
			break;
		case SDK::UTF8:
			ret = CodeConvert::UTF8ToANSI(src, dst);
			break;
		case SDK::UTF8_BOM:
			ret = CodeConvert::UTF8BOMToANSI(src, dst);
			break;
		case SDK::UTF16_LE:
			ret = CodeConvert::UTF16ToANSI(src, dst, true);
			break;
		case SDK::UTF16_BE:
			ret = CodeConvert::UTF16ToANSI(src, dst, false);
			break;
		default:
			break;
		}

		return ret;
	}

	int CodeConvertor::CodeConvertToUTF8(const std::string& src, std::string& dst, EnCodeType srcType)
	{
		int ret = 0;
		std::string midStr;

		switch (srcType)
		{
		case SDK::ANSI:
			ret = CodeConvert::ANSIToUTF8(src, dst);
			break;
		case SDK::UTF8:
			ret = static_cast<int>(src.size());
			dst = src;
			break;
		case SDK::UTF8_BOM:
			CodeConvert::UTF8BOMToANSI(src, midStr);
			ret = CodeConvert::ANSIToUTF8(midStr, dst);
			break;
		case SDK::UTF16_LE:
			CodeConvert::UTF16ToANSI(src, midStr,true);
			ret = CodeConvert::ANSIToUTF8(midStr, dst);
			break;
		case SDK::UTF16_BE:
			CodeConvert::UTF16ToANSI(src, midStr, false);
			ret = CodeConvert::ANSIToUTF8(midStr, dst);
			break;
		default:
			break;
		}

		return ret;
	}

	int CodeConvertor::CodeConvertToUTF8BOM(const std::string& src, std::string& dst, EnCodeType srcType)
	{
		int ret = 0;
		std::string midStr;

		switch (srcType)
		{
		case SDK::ANSI:
			ret = CodeConvert::ANSIToUTF8BOM(src, dst);
			break;
		case SDK::UTF8:
			CodeConvert::UTF8ToANSI(src, midStr);
			ret = CodeConvert::ANSIToUTF8BOM(midStr, dst);
			break;
		case SDK::UTF8_BOM:
			ret = static_cast<int>(src.size());
			dst = src;
			break;
		case SDK::UTF16_LE:
			CodeConvert::UTF16ToANSI(src, midStr, true);
			ret = CodeConvert::ANSIToUTF8BOM(midStr, dst);
			break;
		case SDK::UTF16_BE:
			CodeConvert::UTF16ToANSI(src, midStr, false);
			ret = CodeConvert::ANSIToUTF8BOM(midStr, dst);
			break;
		default:
			break;
		}

		return ret;
	}

	int CodeConvertor::CodeConvertToUTF16LE(const std::string& src, std::string& dst, EnCodeType srcType)
	{
		int ret = 0;
		std::string midStr;

		switch (srcType)
		{
		case SDK::ANSI:
			ret = CodeConvert::ANSIToUTF16(midStr, dst, true);
			break;
		case SDK::UTF8:
			CodeConvert::UTF8ToANSI(src, midStr);
			ret = CodeConvert::ANSIToUTF16(midStr, dst, true);
			break;
		case SDK::UTF8_BOM:
			CodeConvert::UTF8BOMToANSI(src, midStr);
			ret = CodeConvert::ANSIToUTF16(midStr, dst, true);
			break;
		case SDK::UTF16_LE:
			ret = static_cast<int>(src.size());
			dst = src;
			break;
		case SDK::UTF16_BE:
			CodeConvert::UTF16ToANSI(src, midStr, false);
			ret = CodeConvert::ANSIToUTF16(midStr, dst, true);
			break;
		default:
			break;
		}

		return ret;
	}

	int CodeConvertor::CodeConvertToUTF16BE(const std::string& src, std::string& dst, EnCodeType srcType)
	{
		int ret = 0;
		std::string midStr;

		switch (srcType)
		{
		case SDK::ANSI:
			ret = CodeConvert::ANSIToUTF16(midStr, dst, false);
			break;
		case SDK::UTF8:
			CodeConvert::UTF8ToANSI(src, midStr);
			ret = CodeConvert::ANSIToUTF16(midStr, dst, false);
			break;
		case SDK::UTF8_BOM:
			CodeConvert::UTF8BOMToANSI(src, midStr);
			ret = CodeConvert::ANSIToUTF16(midStr, dst, false);
			break;
		case SDK::UTF16_LE:
			CodeConvert::UTF16ToANSI(src, midStr, true);
			ret = CodeConvert::ANSIToUTF16(midStr, dst, false);
			break;
		case SDK::UTF16_BE:
			ret = static_cast<int>(src.size());
			dst = src;
			break;
		default:
			break;
		}

		return ret;
	}

	SDK::EnCodeType CodeConvert::CheckCodeType(const std::string& str, int ChineseNumber)
	{
		size_t size = str.size();
		uint8* data = (uint8*)str.c_str();

		//根据文件头判断编码格式
		//在简体中文Windows操作系统中，ANSI 编码代表 GBK 编码；在英文Windows操作系统中，ANSI 编码代表 ASCII编码；在繁体中文Windows操作系统中，ANSI编码代表Big5。
		if (size > 2 && data[0] == 0xFF && data[1] == 0xFE)
		{
			return EnCodeType::UTF16_LE;
		}
		else if (size > 2 && data[0] == 0xFE && data[1] == 0xFF)
		{
			return EnCodeType::UTF16_BE;
		}
		else if (size > 3 && data[0] == 0xEF && data[1] == 0xBB && data[2] == 0xBF)
		{
			return EnCodeType::UTF8_BOM;
		}

		if (CheckUTF8EncodingFormat(size, data, ChineseNumber))
		{
			return EnCodeType::UTF8;
		}

		return EnCodeType::ANSI;
	}

	int CodeConvert::ANSIToUTF8(const std::string& strAnsi, std::string& strUtf8)
	{
		// 先将 ANSI 编码的字符串转换为 Unicode 编码的字符串
		int nUnicodeLen = MultiByteToWideChar(CP_ACP, 0, strAnsi.c_str(), -1, NULL, 0);
		if (nUnicodeLen <= 0)
			return 0;
		wchar_t* pwszUnicode = new wchar_t[nUnicodeLen];
		if (pwszUnicode == NULL)
		{
			return false;
		}

		nUnicodeLen = MultiByteToWideChar(CP_ACP, 0, strAnsi.c_str(), -1, pwszUnicode, nUnicodeLen);
		if (nUnicodeLen <= 0)
		{
			delete[] pwszUnicode;
			return 0;
		}

		// 再将 Unicode 编码的字符串转换为 UTF-8 编码的字符串
		int nUtf8Len = WideCharToMultiByte(CP_UTF8, 0, pwszUnicode, -1, NULL, 0, NULL, NULL);
		if (nUtf8Len <= 0)
		{
			delete[] pwszUnicode;
			return 0;
		}

		char* pszUtf8 = new char[nUtf8Len];
		if (pszUtf8 == NULL)
		{
			delete[] pwszUnicode;
			return 0;
		}

		nUtf8Len = WideCharToMultiByte(CP_UTF8, 0, pwszUnicode, -1, pszUtf8, nUtf8Len, NULL, NULL);
		if (nUtf8Len <= 0)
		{
			delete[] pszUtf8;
			delete[] pwszUnicode;
			return 0;
		}
		strUtf8 = pszUtf8;
		delete[] pszUtf8;
		delete[] pwszUnicode;
		return nUtf8Len;
	}

	int CodeConvert::UTF8ToANSI(const std::string& strUtf8, std::string& strAnsi)
	{
		// 先将 UTF-8 编码的字符串转换为 Unicode 编码的字符串
		int nLen = MultiByteToWideChar(CP_UTF8, 0, strUtf8.c_str(), -1, NULL, 0);
		if (nLen <= 0)
			return 0;
		wchar_t* pwszUnicode = new wchar_t[nLen];
		if (pwszUnicode == NULL)
		{
			return 0;
		}

		nLen = MultiByteToWideChar(CP_UTF8, 0, strUtf8.c_str(), -1, pwszUnicode, nLen);
		if (nLen <= 0)
		{
			delete[] pwszUnicode;
			return 0;
		}

		// 再将 Unicode 编码的字符串转换为 ANSI 编码的字符串
		nLen = WideCharToMultiByte(CP_ACP, 0, const_cast<const wchar_t*>(pwszUnicode), -1, NULL, 0, NULL, NULL);
		if (nLen <= 0)
		{
			delete[] pwszUnicode;
			return 0;
		}
		strAnsi.resize(nLen - 1);
		nLen = WideCharToMultiByte(CP_ACP, 0, const_cast<const wchar_t*>(pwszUnicode), -1, &strAnsi[0], nLen, NULL, NULL);
		if (nLen <= 0)
		{
			delete[] pwszUnicode;
			return 0;
		}

		delete[] pwszUnicode;
		return nLen - 1;
	}


	int CodeConvert::ANSIToUTF8BOM(const std::string& strAnsi, std::string& strUtf8)
	{
		std::string temp;
		int nUtf8Len = ANSIToUTF8(strAnsi, temp);

		if (nUtf8Len > 0)
		{
			strUtf8 = g_utf8Bom;
			strUtf8 += temp;
			return nUtf8Len + static_cast<int>(g_utf8Bom.size());
		}

		return nUtf8Len;
	}

	int CodeConvert::UTF8BOMToANSI(const std::string& strUTF8Bom, std::string& strAnsi)
	{
		/* strUTF8Bom不满足编码格式，无法转换 */
		if (strUTF8Bom.size() < g_utf8Bom.size())
		{
			return 0;
		}

		/* 去掉BOM标记头 */
		std::string strUtf8 = strUTF8Bom.substr(g_utf8Bom.size());

		return UTF8ToANSI(strUtf8, strAnsi);
	}

	int CodeConvert::ANSIToUTF16(const std::string& strAnsi, std::string& strUtf16, bool isLE)
	{
		// 将 ANSI 编码的字符串转换为 Unicode 编码的字符串
		int nUnicodeLen = MultiByteToWideChar(CP_ACP, 0, strAnsi.c_str(), -1, NULL, 0);
		if (nUnicodeLen <= 0)
		{
			return 0;
		}

		wchar_t* pwszUnicode = new wchar_t[nUnicodeLen];
		if (pwszUnicode == NULL)
		{
			return 0;
		}

		nUnicodeLen = MultiByteToWideChar(CP_ACP, 0, strAnsi.c_str(), -1, pwszUnicode, nUnicodeLen);
		if (nUnicodeLen <= 0)
		{
			delete[] pwszUnicode;
			return 0;
		}

		/* 添加BOM标记 */
		if (isLE)
		{
			strUtf16 += g_utf16LEBom;
		}
		else
		{
			strUtf16 += g_utf16BEBom;

			/* 通过MultiByteToWideChar转换的unicode编码为LE，需要手动转换为BE */
			for (int i = 0; i < nUnicodeLen; i++)
			{
				pwszUnicode[i] = (pwszUnicode[i] << 8) | (pwszUnicode[i] >> 8);
			}
		}

		/* 把unicode编码的字符串拷贝至string */
		std::string unicodeStr;
		unicodeStr.assign((char*)pwszUnicode, nUnicodeLen * 2);

		strUtf16 += unicodeStr;

		return nUnicodeLen;
	}

	int CodeConvert::UTF16ToANSI(const std::string& strUTF16, std::string& strAnsi, bool isLE)
	{
		/* 如果size < 2，则不满足utf16的编码格式，无法进行转换 */
		size_t size = strUTF16.size();
		if (size < 2)
		{
			return 0;
		}

		/* 把utf16编码的字符串从string拷贝至wstring */
		std::wstring wstrUTF16;
		wstrUTF16.resize(size / 2);
		memcpy_s((void*)wstrUTF16.c_str(), size, strUTF16.c_str(), size);

		/* 如果存在BOM，则先去掉BOM，在转换 */
		const char* head = (const char*)(wstrUTF16.c_str());
		if ((head[0] == '\xFF' && head[1] == '\xFE')
			|| (head[0] == '\xFE' && head[1] == '\xFF'))
		{
			wstrUTF16 = wstrUTF16.substr(1);
		}

		/* 如果当前为utf16-BE格式，则需要先转为utf16-LE格式 */
		if (!isLE)
		{
			for (size_t i = 0; i < wstrUTF16.size(); i++)
			{
				wstrUTF16[i] = (wstrUTF16[i] << 8) | (wstrUTF16[i] >> 8);
			}
		}

		/* utf16LE转换为ansi，并存 strAnsi（string）中 */
		int nAnsiCodeLen = WideCharToMultiByte(CP_ACP, 0, wstrUTF16.c_str(), -1, NULL, 0, NULL, NULL);
		if (nAnsiCodeLen <= 0)
		{
			return 0;
		}

		strAnsi.resize(nAnsiCodeLen);
		nAnsiCodeLen = WideCharToMultiByte(CP_ACP, 0, wstrUTF16.c_str(), -1, &strAnsi[0], nAnsiCodeLen, NULL, NULL);
		if (nAnsiCodeLen <= 0)
		{
			return 0;
		}

		return 1;
	}

	inline uint8 GetBit(uint8 value, uint8 n)
	{
		return ((value) & ((uint32)1 << (n)));
	}

	bool CodeConvert::CheckUTF8EncodingFormat(size_t size, uint8* data, int chineseNumber)
	{
		//无文件头根据编码规律来判断编码格式
		//UTF-8的编码规则很简单，只有二条：
		//1）对于单字节的符号，字节的第一位设为0，后面7位为这个符号的unicode码。因此对于英语字母，UTF - 8编码和ASCII码是相同的。
		//2）对于n字节的符号（n>1），第一个字节的前n位都设为1，第n + 1位设为0，后面字节的前两位一律设为10。剩下的没有提及的二进制位，全部为这个符号的unicode码。
		//取第一个字节判断第一位是否为1，以及获取第一位为1时后面位连续为1的数量
		int utf8number = 0;
		for (size_t i = 0; i < size; i++)
		{
			if (GetBit(data[i], 7) >> 7 == 0)
			{
				continue;
			}

			//取一个字节判断第一位是否为1，以及获取第一位为1时后面位连续为1的数量	
			size_t byte = 0;
			for (int j = 7; j >= 0; j--)
			{
				if ((GetBit(data[i], j) >> j) == 0)
				{
					break;
				}

				byte++;
			}

			if (byte == 0)//若byte等于0，则非中文，中文数量清零
			{ 
				utf8number = 0;
				continue;
			}
			else if (byte == 1)//若为1则不遵循utf8编码格式
			{
				return false;
			}
			else if (i + byte > size)//若后续字节不足则也不满足utf8编码格式
			{
				return false;
			}

			/* 判断后续byte-1个字节是否遵循utf8编码规则 */
			for (size_t j = 1; j < byte; j++)
			{
				std::bitset <8> head_bit_match = data[i + j];
				if (!(head_bit_match[7] == 1 && head_bit_match[6] == 0))
				{
					return false;
				}
			}

			/* 若当前中文字符为utf8编码格式则将utf8number++，  */
			utf8number++;
			i += byte - 1;

			/* 如果连续chineseNumber个中文字符都是uft8编码格式或已经是最后一个字符了就认为是utf8 */
			if (utf8number >= chineseNumber || i >= size - 1)
			{
				return true;
			}
		}

		/* 所有字符都满足utf8编码格式，则认为是utf8格式 */
		return true;
	}
}