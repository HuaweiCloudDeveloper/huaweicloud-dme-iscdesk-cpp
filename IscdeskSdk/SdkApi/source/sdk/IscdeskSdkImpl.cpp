/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <winsock2.h>   
#include <memory>
#include <Psapi.h>
#include <iostream>
#include "../../include/thread/Thread.h"
#include "../../include/sdk/IscdeskSdkImpl.h"
#include "../../include/sdkLibrary/Defines.h"
#include "../../include/sdkLibrary/StringConvert.h"
#include "../../include/sdkLibrary/CodeConversion.h"
#include "../../include/sdkLibrary/ProcessOp.h"
#include "../../include/log/Loger.h"
#include "../../include/interface/SdkInvoker.h"

namespace SDK
{
	static std::map<std::string, RecallEvent> g_eventMaps = {
		{"HeartBeatResult",			EVENT_HEARTBEAT_RESULT},
		{"OpenFile",				EVENT_OPENCLOUDFILE_RESULT},
		{"SaveResult",				EVENT_SAVE_RESULT},
		{"Update",					EVENT_UPDATE},
		{"UpgradeNotice",			EVENT_UPGRADENOTICE},
		{"LicenseVerify",			EVENT_LICENSE_VERIFY},
		{"FileUploadStart",			EVENT_UPLOAD_START},
		{"FileUploadEnd",			EVENT_UPLOAD_END}
	};

	std::map<SDK::RecallEvent, SdkMsgConvertPtr> g_translateFuncMaps = {
		{EVENT_LICENSE_VERIFY,			&SdkMsgConvertor::LicenseMsgConvert},
		{EVENT_UPDATE,					&SdkMsgConvertor::UpgradeMsgConvert},
		{EVENT_OPENCLOUDFILE_RESULT,	&SdkMsgConvertor::OpenFileMsgConvert},
		{EVENT_SAVE_RESULT,				&SdkMsgConvertor::SaveFileMsgConvert},
		{EVENT_UPLOAD_END,				&SdkMsgConvertor::SaveFileMsgConvert},
		{EVENT_UPGRADENOTICE,			&SdkMsgConvertor::UpgradeNoticeMsgConvert}
	};

	IscdeskSdkImpl* g_sdkImpl = NULL;//sdk实例对象指针
	std::map<std::string, SDK::RecallEvent> g_shortcutKeyEvent = {
		{"ShortcutSaveToCloud", SDK::EVENT_SHORTCUT_SAVETOCLOUD},
		{"ShortcutSaveAsToCloud", SDK::EVENT_SHORTCUT_SAVEASTOCLOUD}
	};//快捷键对应的事件

	/* windows hook回调函数 */
	void ShortcutKeyRecall(std::string shortcutKeyName)
	{
		GLogInfo("monitor shortcut key [%s]\n", shortcutKeyName.c_str());

		g_sdkImpl->m_threadQueue->PushBack(shortcutKeyName.c_str(), (uint32)shortcutKeyName.size());
	}

	/* 发送心跳 */
	EnThreadLoop HeartBeatFunc(ThreadSleeper& sleeper, void* param)
	{
		IscdeskSdkImpl* impl = (IscdeskSdkImpl*)param;

		// 发送数据
		char sendMsg[SDK_MSG_LEN];
		int len = sprintf_s(sendMsg, SDK_MSG_LEN, "{\"operate\":\"HeartBeat\",\"params\":"
			"{},\"comeBackParams\":{}}");

		if (len < 0)
		{
			GLogError("sprintf_s failed in HeartBeat\n");
		}

		impl->SendMsg(sendMsg);

		/* 心跳每5分钟发送一次 */
		sleeper.Sleep(5 * 60 * 1000);

		return THREAD_LOOP_CONTINUE;
	}

	IscdeskSdkImpl::IscdeskSdkImpl() : IscdeskSdkIdl(), m_clientSocket(INVALID_SOCKET), 
		m_fds({0}), m_portId(0), m_closeEvent(NULL), m_verifyStatus(false), 
		m_isShutOut(false)
	{
		m_sendBuff = (char*)malloc(SDK_MSG_LEN);
		if (m_sendBuff != NULL)
		{
			m_sendBuffSize = SDK_MSG_LEN;
			SecureZeroMemory(m_sendBuff, SDK_MSG_LEN);
		}
		else
		{
			m_sendBuffSize = 0;
		}

		m_replyBuff = (char*)malloc(SDK_RECV_LEN);
		if (m_replyBuff != NULL)
		{
			SecureZeroMemory(m_replyBuff, SDK_RECV_LEN);
		}
		
		/* 创建快捷键操作对象 */
		m_shortcutKeyOp = new ShortcutKeyOp;

		/* 安装hook监控键盘输入操作 */
		if (m_shortcutKeyOp != NULL)
		{
			m_shortcutKeyOp->RegisterWindowsHook(ShortcutKeyRecall);
		}

		/* 监听队列复位 */
		FD_ZERO(&m_fds);

		/* 注册监听函数 */
		m_recvThread.RegisterThreadFunc(this, &IscdeskSdkImpl::RecvReply, NULL);

		/* 注册心跳发送函数 */
		m_heartThread.RegisterThreadFunc(HeartBeatFunc, this);

		/* 注册iscdesk客户端状态检测函数 */
		m_clientCheckThread.RegisterThreadFunc(this, &IscdeskSdkImpl::CheckIscdeskClient, NULL);

		/* 注册重连函数 */
		m_reconnectThread.RegisterThreadFunc(this, &IscdeskSdkImpl::ReconnectIscdesk, NULL);

		/* 注册检查版本更新函数 */
		m_checkNewVersionThread.RegisterThreadFunc(this, &IscdeskSdkImpl::CheckNewVersionsFunc, NULL);

		/* 创建线程池并初始化 */
		m_threadQueue = new ThreadQueue;
		if (m_threadQueue != NULL)
		{
			m_threadQueue->Init(16, SDK_RECV_LEN, 1);
			m_threadQueue->RegisterHandler(this, &IscdeskSdkImpl::ReplyMessageHandle);
		}

		g_sdkImpl = this;
	}

	IscdeskSdkImpl::~IscdeskSdkImpl()
	{
		if (m_threadQueue != NULL)
		{
			delete m_threadQueue;
			m_threadQueue = NULL;
		}

		if (!m_isShutOut)
		{
			ShutOut();
		}
	}

	ResultStatus IscdeskSdkImpl::Init()
	{
		GLogInfo("IscdeskSdk Init Start\n");

		/* 通过共享内存获取appid、portid等信息 */
		if (GetApplicationInfo() == false)
		{
			GLogError("get appid portid failed\n");
			return ERROR_SHM_READ_FAILED;
		}

		/* 初始化Winsock */
		WSADATA wsaData;
		if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		{
			GLogError("initialize Winsock failed\n");
			return ERROR_NETWORK_INITIALIZE_FAILED;
		}

		/* 与 Iscdesk Client进行连接 */
		ResultStatus ret = ConnectClient();
		if (ret != STATUS_SUCCESS)
		{
			return ret;
		}
		
		m_closeEvent = WSACreateEvent();
		WSAEventSelect(m_clientSocket, m_closeEvent, FD_CLOSE);

		/* 启动线程池 */
		if (m_threadQueue != NULL)
		{
			m_threadQueue->Start();
		}

		/* iscdesk客户端状态监听线程启动 */
		m_clientCheckThread.Start();

		/* 回复接收线程启动 */
		m_recvThread.Start();
		
		/* 检查更新线程启动 */
		m_checkNewVersionThread.Start();

		GLogInfo("IscdeskSdkImpl Init success\n");

		return STATUS_SUCCESS;
	}

	ResultStatus IscdeskSdkImpl::ShutOut()
	{
		GLogInfo("IscdeskSdk ShutOut Start\n");

		/* 卸载hook */
		if(m_shortcutKeyOp != NULL)
		{
			m_shortcutKeyOp->UnRegisterWindowsHook();

			delete m_shortcutKeyOp;
			m_shortcutKeyOp = NULL;
		}

		/* 重连线程停止 */
		m_reconnectThread.Stop();

		/* 检查版本升级线程停止 */
		m_checkNewVersionThread.Stop();

		/* 心跳发送线程停止 */
		m_heartThread.Stop();
		
		/* 接受数据线程停止 */
		m_recvThread.Stop();
		
		/* 客户端检查线程停止 */
		m_clientCheckThread.Stop();
		
		/* TCP状态监听事件关闭 */
		if (m_closeEvent != NULL)
		{
			WSACloseEvent(m_closeEvent);
			m_closeEvent = NULL;
		}
		
		/* 关闭socket */
		CloseSocket();

		/* 删除事件回调对象 */
		if (m_eventFunc != NULL)
		{
			delete m_eventFunc;
			m_eventFunc = NULL;
		}

		/* 删除旧版本回调对象 */
		for (auto& its : m_invokerMaps)
		{
			if (its.second != NULL)
			{
				delete its.second;
				its.second = NULL;
			}
		}

		/* 释放消息缓存buff */
		if (m_replyBuff != NULL)
		{
			free(m_replyBuff);
			m_replyBuff = NULL;
		}

		if (m_sendBuff != NULL)
		{
			free(m_sendBuff);
			m_sendBuff = NULL;
		}

		GLogInfo("IscdeskSdk ShutOut finished\n");

		m_isShutOut = true;

		return STATUS_SUCCESS;
	}

	ResultStatus IscdeskSdkImpl::OpenCloudFile()
	{
		std::string sendMsg = "{\"operate\":\"OpenCloudFile\",\"params\":{},\"comeBackParams\":{}}";

		return SendMsg(sendMsg);
	}

	ResultStatus IscdeskSdkImpl::HeartBeat()
	{
		if (!m_heartThread.IsStoped())
		{
			GLogWarning("heart is running\n");

			return STATUS_SUCCESS;
		}

		/* 启动心跳线程 */
		m_heartThread.Start();

		GLogInfo("heartbeat thread start\n");

		return STATUS_SUCCESS;
	}

	ResultStatus IscdeskSdkImpl::SelectSaveAsPath(std::string path, bool isAssAssembly)
	{
		path = StringConvert::CanonicalizePath(path, "\\\\");

		std::lock_guard<std::mutex> lockGuard(m_buffMutex);

		if (!CheckSendBuffSize((int)path.size()))
		{
			return STATUS_FAILURE;
		}

		int len = sprintf_s(m_sendBuff, m_sendBuffSize, "{\"operate\":\"SelectSaveAsPath\",\"params\":"
			"{\"mainFile\":\"%s\", \"assembly\":\"%s\"},\"comeBackParams\":{}}", 
			path.c_str(), StringConvert::BoolToStdString(isAssAssembly).c_str());

		if (len < 0)
		{
			GLogError("sprintf_s failed in SelectSaveAsPath\n");
			return STATUS_FAILURE;
		}

		return SendMsg(m_sendBuff);
	}

	ResultStatus IscdeskSdkImpl::Save(std::string path, bool isAssAssembly)
	{
		path = StringConvert::CanonicalizePath(path, "\\\\");

		std::lock_guard<std::mutex> lockGuard(m_buffMutex);

		if (!CheckSendBuffSize((int)path.size()))
		{
			return STATUS_FAILURE;
		}

		int len = sprintf_s(m_sendBuff, m_sendBuffSize, "{\"operate\":\"Save\",\"params\":"
			"{\"saveFilePath\":\"%s\", \"assembly\":\"%s\"},\"comeBackParams\":{}}", 
			path.c_str(), StringConvert::BoolToStdString(isAssAssembly).c_str());

		if (len < 0)
		{
			GLogError("sprintf_s failed in Save\n");
			return STATUS_FAILURE;
		}

		return SendMsg(m_sendBuff);
	}

	ResultStatus IscdeskSdkImpl::GetSolution(std::string path)
	{
		path = StringConvert::CanonicalizePath(path, "\\\\");

		std::lock_guard<std::mutex> lockGuard(m_buffMutex);

		if (!CheckSendBuffSize((int)path.size()))
		{
			return STATUS_FAILURE;
		}

		int len = sprintf_s(m_sendBuff, m_sendBuffSize, "{\"operate\":\"GetSolution\",\"params\":"
			"{\"filePath\":\"%s\"},\"comeBackParams\":{}}", path.c_str());
		if (len < 0)
		{
			GLogError("sprintf_s failed in GetSolution\n");
			return STATUS_FAILURE;
		}

		return SendMsg(m_sendBuff);
	}

	ResultStatus IscdeskSdkImpl::UpgradeNotify(bool bUpgrade)
	{
		std::string sendMsg;
		if (bUpgrade)
		{
			sendMsg = "{\"operate\":\"Upgrade\",\"params\":{\"status\":\"1\"},\"comeBackParams\":{}}";
		}
		else
		{
			sendMsg = "{\"operate\":\"Upgrade\",\"params\":{\"status\":\"0\"},\"comeBackParams\":{}}";
		}

		return SendMsg(sendMsg);
	}

	ResultStatus IscdeskSdkImpl::CheckNewVersions()
	{
		std::string sendMsg = "{\"operate\":\"CheckNewVersions\",\"params\":{},\"comeBackParams\":{}}";

		return SendMsg(sendMsg);
	}

	SDK::ResultStatus IscdeskSdkImpl::GetLicenseFileRequest(std::string fileName, std::string devInfo)
	{
		std::lock_guard<std::mutex> lockGuard(m_buffMutex);

		if (!CheckSendBuffSize((int)(fileName.size() + devInfo.size())))
		{
			return STATUS_FAILURE;
		}

		int len = sprintf_s(m_sendBuff, m_sendBuffSize, "{\"operate\":\"GetLicenseFileRequest\",\"params\":"
		"{\"licenseFileName\":\"%s\", \"deviceInfo\":\"%s\"},\"comeBackParams\":{}}", 
			fileName.c_str(), devInfo.c_str());
		if (len < 0)
		{
			GLogError("sprintf_s failed in GetLicenseFileRequest\n");
			return STATUS_FAILURE;
		}

		return SendMsg(m_sendBuff);
	}

	SDK::ResultStatus IscdeskSdkImpl::GetLicenseStringRequest(std::string devInfo)
	{
		std::lock_guard<std::mutex> lockGuard(m_buffMutex);

		if (!CheckSendBuffSize((int)devInfo.size()))
		{
			return STATUS_FAILURE;
		}

		int len = sprintf_s(m_sendBuff, m_sendBuffSize, "{\"operate\":\"GetLicenseStringRequest\",\"params\":"
		"{\"deviceInfo\":\"%s\"},\"comeBackParams\":{}}", devInfo.c_str());
		if (len < 0)
		{
			GLogError("sprintf_s failed in GetLicenseStringRequest\n");
			return STATUS_FAILURE;
		}
 
		return SendMsg(m_sendBuff);
	}

	SDK::ResultStatus IscdeskSdkImpl::LicenseVerifyNotify(bool verifyResult, std::string errorMsg)
	{
		/* 调用此通知函数时，先将激活状态保存至内存，以后后续iscdesk主动查询激活状态 */
		m_verifyStatus = verifyResult;
		m_licErrMsg = errorMsg;

		/* 当上次license获取状态为取消（iscdesk中取消获取license枚举值为4）时，不需要通知iscdesk */
		if (m_licStatus == "4")
		{
			return STATUS_SUCCESS;
		}

		std::lock_guard<std::mutex> lockGuard(m_buffMutex);

		if (!CheckSendBuffSize((int)errorMsg.size()))
		{
			return STATUS_FAILURE;
		}

		int len = sprintf_s(m_sendBuff, m_sendBuffSize, "{\"operate\":\"LicenseVerifyNotify\",\"params\":"
			"{\"Result\":\"%s\", \"ErrorMsg\":\"%s\"},\"comeBackParams\":{}}", 
			StringConvert::BoolToStdString(verifyResult).c_str(), errorMsg.c_str());
		if (len < 0)
		{
			GLogError("sprintf_s failed in LicenseVerifyNotify\n");
			return STATUS_FAILURE;
		}

		return SendMsg(m_sendBuff);
	}

	SDK::ResultStatus IscdeskSdkImpl::TrialExpirationNotify()
	{
		std::string sendMsg = "{\"operate\":\"TrialExpirationNotify\",\"params\":{},\"comeBackParams\":{}}";

		return SendMsg(sendMsg);
	}

	SDK::ResultStatus IscdeskSdkImpl::TrialExpirationAndGetLicense(LicenseType licType,
		std::string devInfo, std::string fileName)
	{
		std::lock_guard<std::mutex> lockGuard(m_buffMutex);

		int len;
		if (licType == LICENSE_TYPE_FILE)
		{
			if (!CheckSendBuffSize((int)(fileName.size() + devInfo.size())))
			{
				return STATUS_FAILURE;
			}

			len = sprintf_s(m_sendBuff, m_sendBuffSize, "{\"operate\":\"TrialExpirationNotify\",\"params\":"
				"{\"licenseFileName\":\"%s\", \"deviceInfo\":\"%s\", \"LicenseType\":\"file\"},\"comeBackParams\":{}}",
				fileName.c_str(), devInfo.c_str());
		}
		else
		{
			if (!CheckSendBuffSize((int)(devInfo.size())))
			{
				return STATUS_FAILURE;
			}

			len = sprintf_s(m_sendBuff, m_sendBuffSize, "{\"operate\":\"TrialExpirationNotify\",\"params\":"
				"{\"deviceInfo\":\"%s\" \"LicenseType\":\"string\"},\"comeBackParams\":{}}", devInfo.c_str());
		}

		if (len < 0)
		{
			GLogError("sprintf_s failed in TrialExpirationAndGetLicense\n");
			return STATUS_FAILURE;
		}

		return SendMsg(m_sendBuff);
	}
}
	
/* 私有函数实现 */
namespace SDK
{
	bool IscdeskSdkImpl::GetApplicationInfo()
	{
		HANDLE handle = OpenFileMappingA(FILE_MAP_READ, NULL, SAASCENTERCLIENT_SHM);
		LPVOID ptr = MapViewOfFile(handle, FILE_MAP_READ, 0, 0, 0);
		if (handle == NULL || ptr == NULL)
		{
			GLogError("can not find shared memory [%s]\n", SAASCENTERCLIENT_SHM);
			return false;
		}

		/* 获取port */
		std::string str = StringConvert::ExtractStringBetween((char*)ptr, "\"port\":", ",");
		m_portId = StringConvert::StringToInt(str);

		CloseHandle(handle);

		return true;
	}

	ResultStatus IscdeskSdkImpl::ConnectClient()
	{
		/* 创建socket */
		m_clientSocket = socket(AF_INET, SOCK_STREAM, 0);
		if (m_clientSocket == INVALID_SOCKET)
		{
			GLogError("socket create failed\n");
			WSACleanup();
			return ERROR_NETWORK_INITIALIZE_FAILED;
		}

		int i = 1;
		setsockopt(m_clientSocket, IPPROTO_TCP, TCP_NODELAY, (char*)&i, sizeof(i));

		/* 设置服务器地址和端口号 */
		sockaddr_in serverAddr;
		serverAddr.sin_family = AF_INET;
		serverAddr.sin_addr.s_addr = inet_addr(LOOPBACK_IP);		// 本地地址
		serverAddr.sin_port = htons(m_portId);						// 服务器端口号

		/* 连接服务器 */
		if (connect(m_clientSocket, reinterpret_cast<sockaddr*>(&serverAddr), sizeof(serverAddr)) == SOCKET_ERROR)
		{
			/* 打印错误信息 */
			GLogError("tcp connect remote ip[%s] port[%d] failed\n",
				inet_ntoa(serverAddr.sin_addr), ntohs(serverAddr.sin_port));

			/* 关闭套接字 */
			CloseSocket();

			return ERROR_NETWORK_UNREACHBLE;
		}

		GLogInfo("tcp connect success\n");

		return STATUS_SUCCESS;
	}

	ResultStatus IscdeskSdkImpl::SendMsg(const std::string& msg)
	{
		std::lock_guard<std::mutex> lockGuard(m_reconnectMutex);

		std::string str = msg + "\n";

		/* msg长度超过日志限定长度时进行截断 */
		std::string logStr = str;
		GLogCheckSize(logStr);

		std::string devInfo = StringConvert::ExtractStringBetween(logStr, "\"deviceInfo\":\"", "\"");
		if (devInfo == "")
		{
			GLogInfo("send msg:%s\n", logStr.c_str());
		}
		else
		{
			int size = (int)devInfo.size();

			/* devInfo小于3个字符时，最后一个字符变为'*' */
			if (size < 3)
			{
				devInfo[size - 1] = '*';
			}
			else 
			{
				/* devInfo大于3个字符时，中间1/3部分需要脱敏处理 */
				int startIndex = size / 3;
				int endIndex = 2 * size / 3;
				for (int i = startIndex; i < endIndex; i++)
				{
					devInfo[i] = '*';
				}
			}

			logStr = StringConvert::ReplaceStringBetween(logStr, devInfo, "\"deviceInfo\":\"", "\"");
			GLogInfo("send msg:%s\n", logStr.c_str());
		}

		/* 发送消息 */
		int ret = send(m_clientSocket, str.c_str(), static_cast<int>(str.size()), 0);
		if (ret == SOCKET_ERROR)
		{
			GLogError("SendMsg Failed\n");

			m_reconnectThread.Start();
		}
		else
		{
			GLogInfo("SendMsg Success\n");
		}

		return STATUS_SUCCESS;
	}

	EnThreadLoop IscdeskSdkImpl::RecvReply(ThreadSleeper& sleeper, void* param)
	{
		/* 初始化接收程序 */
		if (!ResetReceive())
		{
			return THREAD_LOOP_EXIT;
		}

		/* 检测是否有数据 */
		if (FD_ISSET(m_clientSocket, &m_fds))
		{
			if (m_replyBuff == NULL)
			{
				m_replyBuff = (char*)malloc(SDK_RECV_LEN);
				if (m_replyBuff == NULL)
				{
					return THREAD_LOOP_CONTINUE;
				}
			}

			/* 接收网络数据之前先把接收缓冲区清空 */
			SecureZeroMemory(m_replyBuff, SDK_RECV_LEN);

			int len = recv(m_clientSocket, m_replyBuff, SDK_RECV_LEN, 0);

			if (len == SOCKET_ERROR)
			{
				int errorCode = WSAGetLastError();
				if (errorCode == WSAEWOULDBLOCK) 
				{
					/* 没有可用的数据进行接收，继续尝试接收操作 */
					GLogWarning("receive failed[no data]\n");
				}
				else if (errorCode == WSAETIMEDOUT) 
				{
					/* 接收超时，可以考虑重新发起接收操作 */
					GLogWarning("receive failed[time out]\n");
				}
				else 
				{
					/* 其他错误处理，例如关闭Socket连接、重新建立连接等 */
					GLogError("receive failed[network error]\n");

					/* 接收线程退出 */
					return THREAD_LOOP_EXIT;
				}

				return THREAD_LOOP_CONTINUE;
			}
			else if (len == 0)
			{
				/* 接收长度为0时代表对端已断开连接 */
				GLogError("receive failed[remote disconnect]\n");

				CloseSocket();

				/* 接收线程退出 */
				return THREAD_LOOP_EXIT;
			}

			/* 打印日志 */
			std::string logStr(m_replyBuff, len);
			GLogCheckSize(logStr);
			GLogInfo("recv:%s\n", logStr.c_str());

			/* 将接收的消息塞入buffer进行处理 */
			m_threadQueue->PushBack(m_replyBuff, len);

			FD_CLR(m_clientSocket, &m_fds);
		}

		return THREAD_LOOP_CONTINUE;
	}

	int IscdeskSdkImpl::ReplyMessageHandle(void* data, uint32 dataLen, uint8 threadId)
	{
		std::string replyMsg((char*)data, dataLen);

		/* 如果回复消息没有operate字段，则说明是快捷键事件，进行快捷键事件处理 */
		if (replyMsg.find("operate") == std::string::npos)
		{
			ShortcutKeyProc(replyMsg);
			return 0;
		}

		std::string operate = StringConvert::ExtractStringBetween(replyMsg, "\"operate\":\"", "\"");
		if (operate == "LicenseVerifyQuery")//license验证结果查询消息由SDK自行处理
		{
			LicenseVerifyNotify(m_verifyStatus, m_licErrMsg);
			return 0;
		}
		else if (operate == "ShortCutKeyConfig")//快捷键配置消息由SDK自行处理
		{
			if (m_shortcutKeyOp != NULL)
			{
				m_shortcutKeyOp->ShortcutKeyConfigParse(replyMsg);
			}
			return -1;
		}

		RecallEvent evt = EventTranslate(operate);
		if (evt == EVENT_LICENSE_VERIFY)
		{
			m_licStatus = StringConvert::ExtractStringBetween(replyMsg, "\"Status\":\"", "\"");
		}
		else if (evt == EVENT_OPENCLOUDFILE_RESULT)
		{
			ProcessOp::SetForegroundMainWindow();
		}

		MessageTranslate(evt, replyMsg);

		std::string logStr = replyMsg;
		GLogCheckSize(logStr);
		GLogInfo("MsgConvert result:%s\n", logStr.c_str());

		/* 处理事件时为了兼容旧版本，首先判断是否已经注册了对应事件的回调函数 */
		if (m_invokerMaps.find(evt) != m_invokerMaps.end()
			&& m_invokerMaps[evt] != NULL)
		{
			m_invokerMaps[evt]->Invoke(replyMsg);
		}
		else if(m_eventFunc != NULL)//没有注册对应事件的回调函数，则判断是否注册事件处理函数
		{
			m_eventFunc->Invoke(evt, replyMsg);
		}
		else
		{
			GLogWarning("recv msg but eventFunc is not register\n");
		}

		return 0;
	}

	bool IscdeskSdkImpl::ResetReceive()
	{
		/* socket集合置位 */
		FD_ZERO(&m_fds);

		/* tcp连接状态未关闭时，才需要监听此socket */
		FD_SET(m_clientSocket, &m_fds);

		/* 监听超时时间设为1s */
		timeval timeout = { 1, 0 };

		/* 监听m_fds，查看是否有事件 */
		int res = select(static_cast<int>(m_clientSocket + 1), &m_fds, NULL, NULL, &timeout);
		if (res < 0)
		{
			GLogError("error select\n");

			return false;
		}

		return true;
	}

	EnThreadLoop IscdeskSdkImpl::CheckIscdeskClient(ThreadSleeper& sleeper, void* param)
	{
		m_evtMutex.lock();

		/* 检测TCP如果发现iscdesk客户端断开连接，且客户端未启动则通过事件回调通知工业软件客户端关闭事件 */
		DWORD dwRet = WaitForSingleObject(m_closeEvent, 0);
		if ((dwRet == WSA_WAIT_EVENT_0) && (ProcessOp::CheckIscdeskClientExecute() < 0))
		{
			GLogInfo("Iscdesk Client disconnect\n");

			/* TCP状态监听事件关闭 */
			WSACloseEvent(m_closeEvent);
			m_closeEvent = NULL;

			m_evtMutex.unlock();

			if (m_closeAppFunc != NULL)
			{
				m_closeAppFunc->Invoke("");
			}
			else if(m_eventFunc != NULL)
			{
				m_eventFunc->Invoke(EVENT_CLIENT_EXIT, "");
			}
		}
		else
		{
			m_evtMutex.unlock();
		}

		sleeper.Sleep(1000);
		return THREAD_LOOP_CONTINUE;
	}

	SDK::RecallEvent IscdeskSdkImpl::EventTranslate(std::string event)
	{
		return g_eventMaps.find(event) != g_eventMaps.end() ? g_eventMaps[event] : EVENT_UNKONW;
	}

	void IscdeskSdkImpl::MessageTranslate(RecallEvent evt, std::string& msg)
	{
		/* iscdesk发送的消息中部分字段值为枚举类型，需要转换为可理解的字符串 */
		auto iter = g_translateFuncMaps.find(evt);
		if (iter != g_translateFuncMaps.end())
		{
			SdkMsgConvertPtr func = iter->second;
			(m_msgConvertor.*func)(msg);
		}
	}

	bool IscdeskSdkImpl::CheckSendBuffSize(int totalSize)
	{
		/* 如果传入的字符串总长度+消息头长度大于buff长度，则需要扩容 */
		totalSize += SDK_MSG_HEAD_LEN;
		if (totalSize > m_sendBuffSize)
		{
			free(m_sendBuff);
			m_sendBuff = (char*)malloc(totalSize);
			if (m_sendBuff == NULL)
			{
				GLogError("error malloc m_sendBuff\n");
				return false;
			}

			m_sendBuffSize = totalSize;

			GLogInfo("m_sendBuff expand to [%d]\n", m_sendBuffSize);
		}

		return true;
	}

	EnThreadLoop IscdeskSdkImpl::ReconnectIscdesk(ThreadSleeper& sleeper, void* param)
	{		
		std::lock_guard<std::mutex> lockGuard(m_reconnectMutex);

		/* 重连时接收线程关闭 */
		m_recvThread.Stop();

		CloseSocket();

		/* 重新连接Iscdesk */
		ResultStatus ret = IscdeskSdkCheckingEnvironment();
		if (ret == STATUS_SUCCESS || ret == ERROR_CLIENT_OFFLINE)
		{
			/* 加锁保证与m_clientCheck线程监听m_closeEvent的同步 */
			m_evtMutex.lock();

			/* TCP状态监听事件关闭 */
			WSACloseEvent(m_closeEvent);
			m_closeEvent = NULL;

			/* 与Iscdesk重新连接 */
			ret = Init();
			
			while (ret != STATUS_SUCCESS)
			{
				if (sleeper.Sleep(1000) == IRQ_THREADKILL)
				{
					break;
				}

				if (ret == ERROR_NETWORK_INITIALIZE_FAILED || ret == ERROR_SHM_READ_FAILED)
				{
					ret = Init();
				}
				else
				{
					ret = ConnectClient();
				}
			}

			m_evtMutex.unlock();

			return THREAD_LOOP_EXIT;
		}

		return THREAD_LOOP_EXIT;
	}

	SDK::EnThreadLoop IscdeskSdkImpl::CheckNewVersionsFunc(ThreadSleeper& sleeper, void* param)
	{
		/* 发送升级检查报文 */
		CheckNewVersions();

		return THREAD_LOOP_EXIT;
	}

	void IscdeskSdkImpl::CloseSocket()
	{
		closesocket(m_clientSocket);
		m_clientSocket = INVALID_SOCKET;
		WSACleanup();
	}

	void IscdeskSdkImpl::ShortcutKeyProc(std::string shortcutKeyName)
	{
		if (shortcutKeyName == "ShortcutOpenCloud")
		{
			g_sdkImpl->OpenCloudFile();
			return;
		}

		auto iter = g_shortcutKeyEvent.find(shortcutKeyName);
		if (iter != g_shortcutKeyEvent.end())
		{
			RecallEvent evt = iter->second;

			/* 判断是否注册事件处理函数 */
			if (g_sdkImpl->m_eventFunc != NULL)
			{
				g_sdkImpl->m_eventFunc->Invoke(evt, "");
			}
			else
			{
				GLogWarning("recv msg but eventFunc is not register\n");
			}
		}
	}
}