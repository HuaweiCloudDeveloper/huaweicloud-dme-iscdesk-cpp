/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#include <map>
#include "../../include/sdkLibrary/StringConvert.h"
#include "../../include/log/Loger.h"
#include "../../include/sdk/SdkMessageConvert.h"

namespace SDK
{
	static std::map<std::string, std::string> g_licenseTypeMaps = {
		{"10",	"file"},				//在Iscdesk中枚举值10代表license为文件格式
		{"11",	"string"}				//在Iscdesk中枚举值11代表license为字符串格式
	};

	static std::map<std::string, std::string> g_licenseStatusMaps = {
		{"2",	"success"},				//在Iscdesk中枚举值2代表license获取成功
		{"3",	"failed"},				//在Iscdesk中枚举值3代表license获取失败
		{"4",	"failed"}				//在Iscdesk中枚举值4代表license获取被取消,但只需告知工业软件failed即可
	};

	static std::map<std::string, std::string> g_openTypeMaps = {
		{"1",	"\"OpenOnly\""},		//在Iscdesk中枚举值1代表仅打开文件
		{"2",	"\"OpenAndCheckout\""}	//在Iscdesk中枚举值2代表打开并检出文件
	};

	static std::map<std::string, std::string> g_saveStatusMaps = {
		{"1",	"\"success\""},			//在Iscdesk中枚举值1代表保存成功
		{"-1",	"\"failed\""}			//在Iscdesk中枚举值-1代表保存失败
	};

	static std::map<std::string, std::string> g_upgradeNoticeAvailableMaps = {
		{"false",	"\"false\""},		//Iscdesk中json格式中false不带双引号，格式统一使用""将值包含
		{"true",	"\"true\""}			//Iscdesk中json格式中true不带双引号，格式统一使用""将值包含
	};

	void SdkMsgConvertor::LicenseMsgConvert(std::string& msg)
	{
		MessageConvert(msg, g_licenseStatusMaps, "\"Status\":\"", "\"");
		MessageConvert(msg, g_licenseTypeMaps, "\"LicenseType\":\"", "\"");
	}

	void SdkMsgConvertor::OpenFileMsgConvert(std::string& msg)
	{
		MessageConvert(msg, g_openTypeMaps, "\"openType\":", ",");
	}

	void SdkMsgConvertor::SaveFileMsgConvert(std::string& msg)
	{
		MessageConvert(msg, g_saveStatusMaps, "\"status\":", "}");
	}

	void SdkMsgConvertor::UpgradeMsgConvert(std::string& msg)
	{
		if (msg.find("fromForceWin") != std::string::npos)
		{
			/* 当fromForceWin字段的值不为字符串时说明此次升级是非强制升级，将值设为"false" */
			if (StringConvert::ExtractStringBetween(msg, "\"fromForceWin\":\"", "\"") == "")
			{
				msg = "{\"operate\":\"Update\",\"params\":{\"fromForceWin\":\"false\"},\"comeBackParams\":{}}";
			}
		}
		else
		{
			/* 当fromForceWin字段不存在时表明此次消息是强制升级界面被关闭，同强制升级消息相同 */
			msg = "{\"operate\":\"Update\",\"params\":{\"fromForceWin\":\"true\"},\"comeBackParams\":{}}";
		}
	}

	void SdkMsgConvertor::UpgradeNoticeMsgConvert(std::string& msg)
	{
		MessageConvert(msg, g_upgradeNoticeAvailableMaps, "\"available\":", "}");
	}

	void SdkMsgConvertor::MessageConvert(
		std::string& msg,
		const std::map<std::string, std::string>& table,
		const std::string& left, 
		const std::string& right)
	{
		std::string value = StringConvert::ExtractStringBetween(msg, left, right);

		for (const auto& its : table)
		{
			if (its.first == value)
			{
				msg = StringConvert::ReplaceStringBetween(msg, its.second, left, right);
				break;
			}
		}
	}
}