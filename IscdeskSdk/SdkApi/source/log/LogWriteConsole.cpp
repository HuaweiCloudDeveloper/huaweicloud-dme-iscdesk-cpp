/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#include "../../include/log/LogWriteConsole.h"

namespace SDK
{
	void LogWriteConsole::WriteToConsole(const std::string& logContent)
	{
		printf("%s", logContent.c_str());
	}
}