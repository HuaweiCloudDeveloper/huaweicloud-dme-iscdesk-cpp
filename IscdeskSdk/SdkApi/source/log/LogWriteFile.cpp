/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#define _CRT_SECURE_NO_WARNINGS 
#include <windows.h>
#include "../../include/log/LogWriteFile.h"

namespace SDK
{
	int g_logCutSize = 10485760;

	SDK::LogWriteFile::LogWriteFile(std::string logName, std::string logDir)
		:m_logName(logName), m_logDir(logDir), m_cutBySize(true), m_maxSize(g_logCutSize)
	{
		std::string filePath = logDir + "/" + logName;
		struct stat fileStates;
		stat(filePath.c_str(), &fileStates);

		if (fileStates.st_size == 0)
		{
			m_fp = fopen(filePath.c_str(), "w+");
			m_currentSize = 0;
		}
		else
		{
			m_fp = fopen(filePath.c_str(), "a+"); 
			m_currentSize = fileStates.st_size;
		}
	}

	LogWriteFile::~LogWriteFile()
	{
		if (m_fp != NULL)
		{
			fclose(m_fp);
		}
	}

	void LogWriteFile::SetCutSize(int cutSize)
	{
		if (cutSize > 0)
		{
			m_maxSize = cutSize;
		}
	}

	void LogWriteFile::WriteFile(const std::string& logContent)
	{
		if (m_fp != NULL)
		{
			std::lock_guard<std::mutex> lockGuard(m_mutex);

			if(m_cutBySize && m_currentSize > m_maxSize)
			{
				Clear();
			}

			fputs(logContent.c_str(), m_fp);
			m_currentSize += (int)logContent.size();
			fflush(m_fp);
		}
	}

	void LogWriteFile::Clear()
	{
		fclose(m_fp);

		/* 清空日志文件时把当前日志复制为sdk_old.log */
		std::string newLogPath = m_logDir + "/" + m_logName;
		std::string oldLogPath = m_logDir + "/" + "sdk_old.log";
		CopyFile(newLogPath.c_str(), oldLogPath.c_str(), false);
		
		m_fp = fopen((m_logDir + "/" + m_logName).c_str(), "w+");

		m_currentSize = 0;
	}

}