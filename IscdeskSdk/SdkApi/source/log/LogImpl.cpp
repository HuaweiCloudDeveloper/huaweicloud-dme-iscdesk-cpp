/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#include "../../include/log/LogImpl.h"
#include "../../include/sdkLibrary/Defines.h"

namespace SDK
{
	LogImpl::LogImpl()
	{
		/* 创建写文件对象 */
		m_fileWriter = new LogWriteFile;
		
		/* 创建写控制台对象 */
		m_consoleWriter = new LogWriteConsole;

		/* 设置日志展示等级（低于此等级的日志信息将不会进行显示） */
		SetLogShowLevel(SDKLOG_DEBUG);
	}

	LogImpl::~LogImpl()
	{
		if (m_fileWriter != NULL)
		{
			delete m_fileWriter;
			m_fileWriter = NULL;
		}

		if (m_consoleWriter != NULL)
		{
			delete m_consoleWriter;
			m_consoleWriter = NULL;
		}
	}

	void LogImpl::WriteLog(LogLevel logLevel, const std::string& logContent)
	{
		if (logLevel < m_logShowLevel)
		{
			return;
		}

		const int logLen = SDK_LOG_LEN + SDK_MSG_HEAD_LEN;//日志长度最大为限定长度+消息头长度
		char buff[logLen];
		int len = sprintf_s(buff, logLen, "[%s] %-8s%s", Date().GetCurrentDate().c_str(), 
			LogLevelConvert(logLevel).c_str(), logContent.c_str());
		if(len < 0)
		{
			return;
		}

		if (m_consoleWriter != NULL)
		{
			m_consoleWriter->WriteToConsole(buff);
		}

		if (m_fileWriter != NULL)
		{
			m_fileWriter->WriteFile(buff);
		}
	}

	void LogImpl::SetLogShowLevel(LogLevel logShowLevel)
	{
		m_logShowLevel = logShowLevel;
	}
	 
	void LogImpl::CheckLogSize(std::string& logStr)
	{
		if (logStr.size() > SDK_LOG_LEN)
		{
			char buff[SDK_MSG_HEAD_LEN];
			sprintf_s(buff, SDK_MSG_HEAD_LEN, "log len [%Iu] is over [%d] will truncate\n",
				 logStr.size(), SDK_LOG_LEN);

			WriteLog(SDKLOG_WARN, buff);

			/* 截取前SDK_LOG_LEN字符的日志 */
			logStr = logStr.substr(0, SDK_LOG_LEN);
		}
	}
}
