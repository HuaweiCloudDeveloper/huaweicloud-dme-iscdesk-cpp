#include "../../include/log/Loger.h"
#include "../../include/thread/ThreadQueue.h"

namespace SDK
{
	ThreadQueue::~ThreadQueue()
	{
		if (m_handleGroup != NULL)
		{
			delete m_handleGroup;
			m_handleGroup = NULL;
		}

		if (m_ringBuffer != NULL)
		{
			delete m_ringBuffer;
			m_ringBuffer = NULL;
		}
	}

	void ThreadQueue::Init(uint16 nodeNum, uint32 nodeSize, uint8 threadNum)
	{
		m_ringBuffer = new RingBuffer(nodeNum, nodeSize);
		if (m_ringBuffer == NULL)
		{
			return;
		}

		m_handleGroup = new HandlerGroup(m_ringBuffer, threadNum);
		if (m_handleGroup == NULL)
		{
			return;
		}

		m_ringBuffer->RegisterHandlerGroup(m_handleGroup);
	}

	int ThreadQueue::PushBack(const void* data, uint32 len)
	{
		if (m_ringBuffer == NULL || m_handleGroup == NULL)
		{
			GLogError("error m_ringBuff/m_handleGroup is NULL\n");
			return -1;
		}

		uint32 bufferSize = m_ringBuffer->NodeSize() - sizeof(RBNode);
		if (len > bufferSize)
		{
			GLogError("error pushback data size > node size\n");
			return -1;
		}

		uint32 wseq;
		if (m_ringBuffer->TryNext(wseq) < 0)
		{
			GLogError("error pushback data , ringbuffer is full\n");
			return -1;
		}

		RBNode* node = m_ringBuffer->GetRBNode(wseq);
		memcpy_s(node->m_data, bufferSize, data, len);
		m_ringBuffer->Commit(wseq, len);

		m_handleGroup->DataNotify();

		return 0;
	}

	void ThreadQueue::Start()
	{
		if (m_handleGroup != NULL)
		{
			m_handleGroup->Start();
		}
	}
}