/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */
#include "../../include/thread/ThreadSema.h"

namespace SDK
{
	SDK::ThreadSema::ThreadSema()
	{
		THREAD_SEMA_INIT(m_sema);
	}

	ThreadSema::~ThreadSema()
	{
		THREAD_SEMA_DESTORY(m_sema);
	}

	int ThreadSema::Post()
	{
		return THREAD_SEMA_POST(m_sema);
	}

	int ThreadSema::Wait()
	{
		return THREAD_SEMA_WAIT(m_sema);
	}

	int ThreadSema::TryWait(int ms)
	{
		return THREAD_SEMA_TRYWAIT(m_sema, ms);
	}

	void ThreadSema::Reset()
	{
		THREAD_SEMA_DESTORY(m_sema);
		THREAD_SEMA_INIT(m_sema);
	}
}
