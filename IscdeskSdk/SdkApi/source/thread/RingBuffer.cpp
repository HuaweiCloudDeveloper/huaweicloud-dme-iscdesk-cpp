#include <iostream>
#include "../../include/log/Loger.h"
#include "../../include/thread/RingBuffer.h"
#include "../../include/thread/HandlerGroup.h"

namespace SDK
{
	RingBuffer::RingBuffer(uint16 num, uint32 size)
		:m_nodeNum(num), m_writeSeq(-1), m_handlerGroup(NULL)
	{
		m_nodeSize = size + sizeof(RBNode);
		uint32 totalSize = num * (size + sizeof(RBNode));

		m_buff = (char*)malloc(totalSize);
		if (m_buff != NULL)
		{
			SecureZeroMemory(m_buff, totalSize);
		}
	}

	RingBuffer::~RingBuffer()
	{
		if (m_buff != NULL)
		{
			free(m_buff);
			m_buff = NULL;
		}
	}

	void RingBuffer::RegisterHandlerGroup(HandlerGroup* handlerGroup)
	{
		m_handlerGroup = handlerGroup;
	}

	int RingBuffer::TryNext(uint32& wSeq)
	{
		if (m_handlerGroup == NULL)
		{
			GLogError("error RingBuffer note register HandlerGroup\n");
			return -1;
		}

		std::lock_guard<std::mutex> lk(m_mutex);

		int next = m_writeSeq + 1;
		if (m_handlerGroup->IsCanWrite(next))
		{
			m_writeSeq++;
			wSeq = next;
			return 0;
		}

		return -1;
	}

	void RingBuffer::Commit(uint32 seq, int dataLen)
	{
		RBNode* node = GetRBNode(seq);
		if (node != NULL)
		{
			node->m_len = dataLen;
			node->m_flag = 1;
		}
	}

	bool RingBuffer::IsAvailable(uint32 seq)
	{
		RBNode* node = GetRBNode(seq);
		if (node == NULL)
		{
			return false;
		}

		return node->m_flag == 1;
	}

	SDK::uint16 RingBuffer::NodeNum()
	{
		return m_nodeNum;
	}

	SDK::uint32 RingBuffer::NodeSize()
	{
		return m_nodeSize;
	}

	SDK::RBNode* RingBuffer::GetRBNode(uint32 seq)
	{
		if (m_buff == NULL)
		{
			return NULL;
		}

		seq = seq % m_nodeNum;
		return reinterpret_cast<RBNode*>(m_buff + seq * m_nodeSize);
	}
}