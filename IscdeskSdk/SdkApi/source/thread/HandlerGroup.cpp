#include "../../include/log/Loger.h"
#include "../../include/thread/HandlerGroup.h"

namespace SDK
{
	HandlerGroup::HandlerGroup(RingBuffer* ringBuffer, uint8 threadNum)
		:m_ringBuffer(ringBuffer), m_ivker(NULL), m_readSeq(-1)
	{
		for (auto i = 0; i < threadNum; i++)
		{
			WaitStrategy* waitStrategy = new WaitStrategy;
			if (waitStrategy == NULL)
			{
				continue;
			}

			m_waitStrategys.push_back(waitStrategy);
			DataHandler* dataHandler = new DataHandler(this, m_ringBuffer, waitStrategy, i);
			if (dataHandler != NULL)
			{
				RegisterDataHandler(dataHandler);
			}
		}
	}

	HandlerGroup::~HandlerGroup()
	{
		Stop();

		if (m_ivker != NULL)
		{
			delete m_ivker;
			m_ivker = NULL;
		}

		for (size_t i = 0; i < m_handlers.size(); i++)
		{
			if (m_handlers[i] != NULL)
			{
				delete m_handlers[i];
				m_handlers[i] = NULL;
			}
		}

		for (size_t i = 0; i < m_waitStrategys.size(); i++)
		{
			if (m_waitStrategys[i] != NULL)
			{
				delete m_waitStrategys[i];
				m_waitStrategys[i] = NULL;
			}
		}
	}

	void HandlerGroup::RegisterDataHandler(DataHandler* dataHandler)
	{
		m_handlers.push_back(dataHandler);
	}

	void HandlerGroup::Start()
	{
 		for (size_t i = 0; i < m_handlers.size(); i++)
 		{
 			m_handlers[i]->Start();
 		}
	}

	void HandlerGroup::Stop()
	{
		for (size_t i = 0; i < m_handlers.size(); i++)
		{
			m_handlers[i]->Stop();
		}
	}

	bool HandlerGroup::IsCanWrite(uint32 wseq)
	{
		if (wseq - m_readSeq > m_ringBuffer->NodeNum())
		{
			return false;
		}

		if (m_ringBuffer->IsAvailable(wseq))
		{
			return false;
		}

		return true;
	}

	void HandlerGroup::DataNotify()
	{
		for (size_t i = 0; i < m_handlers.size(); i++)
		{
			if (m_handlers[i]->IsBusy())
			{
				continue;
			}

			m_handlers[i]->m_waitStrategy->Interrupt();
			break;
		}
	}
}