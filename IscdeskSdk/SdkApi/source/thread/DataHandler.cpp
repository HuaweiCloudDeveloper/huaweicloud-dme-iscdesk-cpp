#include "../../include/log/Loger.h"
#include "../../include/thread/DataHandler.h"
#include "../../include/thread/HandlerGroup.h"

namespace SDK
{
	bool WaitStrategy::WaitData(DataHandler* dataHandler)
	{
		while (!dataHandler->WaitCondition())
		{
			/* 中断等待说明等不到数据，返回失败 */
			if (m_sleeper.GetIrq() == IRQ_THREADKILL)
			{
				return false;
			}

			/* 如果等不到数据则一直等,直到被中断 */
			//中断可能是有新数据到来，也可能是线程需要停止了
			//新数据到来后需要再次竞争
			m_sleeper.Sleep();
		}

		/* 正常等到数据则返回成功 */
		return true;
	}

	void WaitStrategy::Start()
	{
		m_sleeper.Reset();
	}

	void WaitStrategy::Stop()
	{
		m_sleeper.Interrupt(IRQ_THREADKILL);
	}

	void WaitStrategy::Interrupt()
	{
		m_sleeper.Interrupt();
	}

}

namespace SDK
{
	DataHandler::DataHandler(HandlerGroup* handlerGroup, RingBuffer* ringBuffer, 
		WaitStrategy* waitStrategy, uint8 threadId) :m_handlerGroup(handlerGroup), m_ringBuffer(ringBuffer),
		m_waitStrategy(waitStrategy), m_readSeq(-1), m_isOccupy(false), m_threadId(threadId)
	{
		m_handleThread.RegisterThreadFunc(this, &DataHandler::DataProcess, NULL);
	}

	DataHandler::~DataHandler()
	{
		Stop();
	}

	void DataHandler::Start()
	{
		m_handleThread.Start();
	}

	void DataHandler::Stop()
	{
		/* 防止等待策略死等 */
		m_waitStrategy->Stop();

		/* 关闭睡眠器 */
		m_sleeper.Interrupt(IRQ_THREADKILL);

		m_handleThread.Stop();
	}

	bool DataHandler::IsBusy()
	{
		return m_isOccupy;
	}

	EnThreadLoop DataHandler::DataProcess(ThreadSleeper& sleeper, void* param)
	{
		if (m_handlerGroup->m_ivker == NULL)
		{
			GLogError("error thread process func is NULL\n");
			return THREAD_LOOP_EXIT;
		}

		m_isOccupy = false;
		/* 如果返回失败则说明需要中断线程 */
		if (!m_waitStrategy->WaitData(this))
		{
			return THREAD_LOOP_EXIT;
		}

		m_isOccupy = true;
		RBNode* node = m_ringBuffer->GetRBNode(m_readSeq);

		/* 等待数据拷贝、提交完成，防止在数据未拷贝完全时使用到脏数据 */
		while (m_sleeper.GetIrq() != IRQ_THREADKILL)
		{
			if (node->m_flag == 1)
			{
				break;
			}
		}

		m_handlerGroup->m_ivker->Invoke(node->m_data, node->m_len, m_threadId);
		node->m_flag = 0;

		return THREAD_LOOP_CONTINUE;
	}

	bool DataHandler::WaitCondition()
	{
		std::lock_guard<std::mutex> lk(m_handlerGroup->m_mutex);

		/* 如果读序号追上了写序号，则不具备读取条件 */
		uint32 rseq = m_handlerGroup->m_readSeq;
		uint32 wseq = m_ringBuffer->m_writeSeq;
		if (rseq == wseq)
		{
			return false;
		}

		/* 拿到一个数据后就将处理组中的读序号+1，并将此序号保存 */
		m_handlerGroup->m_readSeq++;
		m_readSeq = m_handlerGroup->m_readSeq;

		return true;
	}
}
