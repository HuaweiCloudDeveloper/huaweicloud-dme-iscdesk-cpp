/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#include <process.h>
#include <iostream>
#include "../../include/thread/Thread.h"
#include "../../include/log/Loger.h"

namespace SDK
{
#ifdef _WIN32

	typedef unsigned int(__stdcall *THREAD_CALLBACK)(void*);

	/* 线程创建函数 */
	int ThreadCreate(HANDLE& threadHandle, THREAD_CALLBACK call, void* param)
	{
		unsigned int threadId;
		threadHandle = (HANDLE)_beginthreadex(NULL, 0, call, param, 0, &threadId);

		if (threadHandle != 0)
		{
			return 0;
		}

		return -1;
	}

	/* 线程调用函数 */
	unsigned int __stdcall ThreadCall(void* ptr)
	{
		ThreadIdl* pThread = (ThreadIdl*)ptr;

		/* 调用run函数，线程运行 */
		pThread->Run();

		/* 线程函数运行完毕后将标志位置位 */
		pThread->m_bStoped = true;

		GLogInfo("thread[%s] exit\n", pThread->m_threadName.c_str());

		return 0;
	}
#endif
}

namespace SDK
{
	/* 线程idl基类*/
	ThreadIdl::ThreadIdl(const std::string &name) :m_bShouldExit(false), m_bStoped(true),
		m_threadName(name), m_threadHandle(INVALID_HANDLE_VALUE)
	{

	}

	ThreadIdl::~ThreadIdl()
	{
		Stop();
	}

	int ThreadIdl::Start()
	{
		int ret = 0;

		/* 只有线程当前为停止状态才能启动 */
		if (m_bStoped)
		{
			/* 将线程退出标志置为false */
			m_bShouldExit = false;

			/* 初始化线程睡眠器和信号量 */
			m_sleeper.Reset();		//睡眠器重置

			/* 创建线程 */
			ret = ThreadCreate(m_threadHandle, ThreadCall, this);
			if (ret == 0)
			{
				/* 将线程停止状态置为false */
				m_bStoped = false;
			}
		}

		return ret;
	}

	int ThreadIdl::Stop(int ms)
	{
		/* 只有线程未关闭才能进入Stop逻辑 */
		if (!m_bStoped)
		{
			/* 将线程退出标志置为true */
			m_bShouldExit = true;

			/* 线程睡眠器中断，通知线程退出 */
			m_sleeper.Interrupt(IRQ_THREADKILL);

			/* 等待线程执行完毕，关闭线程句柄 */
			THREAD_JOIN(m_threadHandle);
			THREAD_CLOSE(m_threadHandle);	

			/* 将线程停止状态置为true */
			m_bStoped = true;
		}

		return 0;
	}

	bool ThreadIdl::IsStoped()
	{
		return m_bStoped;
	}
}

namespace SDK
{
	ThreadInvker::ThreadInvker(const std::string& name)
		: ThreadIdl(name), m_invoker(NULL), m_param(NULL)
	{

	}

	ThreadInvker::~ThreadInvker()
	{
		if (m_invoker != NULL)
		{
			delete m_invoker;
			m_invoker = NULL;
		}
	}

	int ThreadInvker::Run()
	{
		if (m_invoker == NULL)
		{
			return THREAD_LOOP_EXIT;
		}

		while (true)
		{
			if (m_bShouldExit)
			{
				break;
			}

			if (m_invoker->Invoke(m_sleeper, m_param) == THREAD_LOOP_EXIT)
			{
				break;
			}
		}

		return 0;
	}
}


