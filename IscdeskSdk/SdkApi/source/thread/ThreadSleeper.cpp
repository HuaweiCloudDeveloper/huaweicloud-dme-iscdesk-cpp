/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#include "../../include/thread/ThreadSleeper.h"
#include <iostream>
namespace SDK
{
	void ThreadSleeper::Reset()
	{
		m_timeSema.Reset();
		m_irq = IRQ_NULL;
	}

	SleepIRQ ThreadSleeper::Sleep()
	{
		return ThreadSleep(false, 0);
	}

	SleepIRQ ThreadSleeper::Sleep(int ms)
	{
		return ThreadSleep(true, ms);
	}

	void ThreadSleeper::Interrupt(SleepIRQ irq)
	{
		if (m_irq != IRQ_THREADKILL)
		{
			m_irq = irq;

			/* 唤醒线程睡眠器 */
			m_timeSema.Post();
		}
	}

	bool ThreadSleeper::IsSleep()
	{
		return m_sleeping;
	}

	SleepIRQ ThreadSleeper::GetIrq()
	{
		return m_irq;
	}

	SleepIRQ ThreadSleeper::ThreadSleep(bool btrywait, int ms)
	{
		SleepIRQ ret = IRQ_NULL;

		/* IRQ_THREADKILL信号之后，除了重置，否则无法再进入sleep */
		if (m_irq == IRQ_THREADKILL)
		{
			return m_irq;
		}

		/* 进入睡眠 */
		m_sleeping = true;

		if (btrywait)
		{
			if (m_timeSema.TryWait(ms) < 0)
			{
				//超时被唤醒
				ret = IRQ_TIMEOUT;
			}
		}
		else
		{
			if (m_timeSema.Wait() < 0)
			{
				//被莫名唤醒
				ret = IRQ_ELSE;
			}
		}

		/* 睡眠结束 */
		m_sleeping = false;

		/* m_irq不为IRQ_NULL时，说明是通过Interrupt结束sleep */
		if (m_irq != IRQ_NULL)
		{
			ret = m_irq;
		}

		/* 除IRQ_THREADKILL外，在Sleep结束后需要将中断置位 */
		if (m_irq != IRQ_THREADKILL)
		{
			m_irq = IRQ_NULL;
		}

		return ret;
	}
}
