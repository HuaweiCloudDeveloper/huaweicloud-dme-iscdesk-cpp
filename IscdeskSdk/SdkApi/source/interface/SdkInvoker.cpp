/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#include <winsock2.h>   
#include <process.h>
#include <tlhelp32.h>
#include <Psapi.h>
#include <iostream>
#include <mutex>
#include "../../include/interface/SdkInvoker.h"
#include "../../include/thread/ThreadSema.h"
#include "../../include/sdk/IscdeskSdkImpl.h"
#include "../../include/sdkLibrary/StringConvert.h"
#include "../../include/log/Loger.h"
#include "../../include/sdkLibrary/ProcessOp.h"

namespace SDK
{
	IscdeskSdkIdl*	g_iscdeskSdkObj = NULL;
	std::mutex		g_mutex;

	IscdeskSdkIdl* IscdeskFactory()
	{
		if (g_iscdeskSdkObj == NULL)
		{
			std::lock_guard<std::mutex> lockGuard(g_mutex);

			if (g_iscdeskSdkObj == NULL)
			{
				g_iscdeskSdkObj = new IscdeskSdkImpl;

				GLogInfo("IscdeskSdkImpl obj creat success\n");

				return g_iscdeskSdkObj;
			}
			else
			{
				return g_iscdeskSdkObj;
			}
		}
		else
		{
			return g_iscdeskSdkObj;
		}
	}

	DLL_API void DestoryIscdesk()
	{
		if (g_iscdeskSdkObj != NULL)
		{
			std::lock_guard<std::mutex> lockGuard(g_mutex);
			if(g_iscdeskSdkObj != NULL)
			{
				delete g_iscdeskSdkObj;
				g_iscdeskSdkObj = NULL;

				GLogInfo("IscdeskSdkImpl obj destory\n");
			}
		}
	}

	DLL_API	ResultStatus IscdeskSdkCheckingEnvironment()
	{
		GLogInfo("Iscdesk sdk environment check start\n");

		/* 获取iscdesk客户端的安装路径 */
		std::string saasPath;
		if (ProcessOp::GetIscdeskClientInstallPath(saasPath) < 0)
		{
			return ERROR_CLIENT_NOINSTALL;
		}

		GLogInfo("Iscdesk Client is Installed\n");

		/* 检查IscdeskClient是否正在运行 */
		if (ProcessOp::CheckIscdeskClientExecute() == 0)
		{
			return STATUS_SUCCESS;
		}
	
		/* 如果iscdesk客户端没启动，则sdk启动iscdesk客户端 */
		GLogInfo("Iscdesk Client execute start\n");
		CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
				 
		SHELLEXECUTEINFO shExInfo = { 0 };
		shExInfo.cbSize = sizeof(SHELLEXECUTEINFO);
		shExInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
		shExInfo.hwnd = NULL;
		shExInfo.lpFile = saasPath.c_str();
		shExInfo.lpDirectory = NULL;
		shExInfo.nShow = SW_SHOW;
				 
		/* 如果启动进程失败，则返回错误码：SaaS客户端未启动 */
		if (ShellExecuteEx(&shExInfo) == false)
		{
			GLogError("Iscdesk Client execute failed\n");
			return STATUS_FAILURE;
		}

		GLogInfo("Iscdesk sdk environment check end\n");

		return ERROR_CLIENT_OFFLINE;
	}
}