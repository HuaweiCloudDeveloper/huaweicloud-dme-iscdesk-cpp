/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef SDK_INVOKER_H
#define SDK_INVOKER_H

#include <windows.h> 
#include "../sdkLibrary/Defines.h"
#include "../sdk/IscdeskSdkIdl.h"

/* 外部调用接口 */
namespace SDK
{
	typedef IscdeskSdkIdl*	(*FuncPtrIscdeskFactory)();
	typedef void			(*FuncPtrDestoryIscdesk)();
	typedef ResultStatus	(*FuncPtrCheckingEnvironment)();

	class DLL_API SdkInvoker
	{
	public:
		explicit SdkInvoker(HMODULE sdkHandle) :m_handle(sdkHandle) {}

		/* IscdeskSdkIdl对象创建工厂 */
		IscdeskSdkIdl* IscdeskFactory()
		{
			FuncPtrIscdeskFactory func = (FuncPtrIscdeskFactory)GetProcAddress(m_handle, "IscdeskFactory");

			if (func != NULL)
			{
				return func();
			}
			else
			{
				return NULL;
			}
		}
		
		/* IscdeskSdkIdl对象销毁 */
		void DestoryIscdesk()
		{
			FuncPtrDestoryIscdesk func = (FuncPtrDestoryIscdesk)GetProcAddress(m_handle, "DestoryIscdesk");

			if (func != NULL)
			{
				func();
			}
		}

		/* SDK环境检查 */
		ResultStatus IscdeskSdkCheckingEnvironment()
		{
			FuncPtrCheckingEnvironment func = (FuncPtrCheckingEnvironment)GetProcAddress(m_handle, "IscdeskSdkCheckingEnvironment");

			if (func != NULL)
			{
				return func();
			}

			return ERROR_INTERFACE_INVALID;
		}

	private:
		HMODULE		m_handle;
	};

	extern "C"
	{
		DLL_API IscdeskSdkIdl*	IscdeskFactory();
		DLL_API void			DestoryIscdesk();
		DLL_API	ResultStatus	IscdeskSdkCheckingEnvironment();
	}
}

#endif
