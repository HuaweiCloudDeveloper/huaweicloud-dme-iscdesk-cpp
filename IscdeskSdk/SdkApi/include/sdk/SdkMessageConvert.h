/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef SDK_MESSAGE_CONVERT_H
#define SDK_MESSAGE_CONVERT_H

#include <string>

namespace SDK
{
	class SdkMsgConvertor
	{
	public:
		/* License验证消息跟据上游Iscdesk发送的枚举值进行转换 */
		void LicenseMsgConvert(std::string& msg);

		/* 打开云端文件消息跟据上游Iscdesk发送的枚举值进行转换 */
		void OpenFileMsgConvert(std::string& msg);

		/* 文件保存结果消息跟据上游Iscdesk发送的枚举值进行转换 */
		void SaveFileMsgConvert(std::string& msg);

		/* 升级消息跟据上游Iscdesk发送的枚举值进行转换 */
		void UpgradeMsgConvert(std::string& msg);

		/* 升级通知消息添加"" */
		void UpgradeNoticeMsgConvert(std::string& msg);

	private:
		/* 根据映射表tables转换msg消息中指定字段的值 */
		void MessageConvert(
			std::string& msg,
			const std::map<std::string, std::string>& table,
			const std::string& left, 
			const std::string& right
		);
	};

	using SdkMsgConvertPtr = void (SdkMsgConvertor::*)(std::string&);

}
 
#endif