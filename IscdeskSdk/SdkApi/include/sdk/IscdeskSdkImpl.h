/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef SDK_IMPL_HEAD_H
#define SDK_IMPL_HEAD_H

#include <winsock2.h>
#include <map>
#include <vector>
#include <mutex>
#include "../sdkLibrary/Defines.h"
#include "../sdkLibrary/CodeConversion.h"
#include "../sdkLibrary/ShortcutKeyOp.h"
#include "../thread/Thread.h"
#include "../thread/ThreadQueue.h"
#include "SdkMessageConvert.h"
#include "IscdeskSdkIdl.h"

namespace SDK
{
	class IscdeskSdkImpl :public IscdeskSdkIdl
	{
		friend EnThreadLoop HeartBeatFunc(ThreadSleeper& sleeper, void* param);
		friend void ShortcutKeyRecall(std::string shortcutKeyName);

	public:
		IscdeskSdkImpl();
		IscdeskSdkImpl(const IscdeskSdkImpl&) = delete;
		void operator=(IscdeskSdkImpl&) = delete;

		~IscdeskSdkImpl();

		/**
		* 初始化函数，必须首先调用，建立连接与初始化逻辑处理
		*
		*/
		ResultStatus Init();

		/**
		* 退出函数，程序关闭之前调用。断开连接与释放资源
		*
		*/
		ResultStatus ShutOut();

		/**
		* 心跳函数，工业软件与iscdesk保持心跳
		*
		*/
		ResultStatus HeartBeat();

		/**
		* 打开云端文件
		*
		*/
		ResultStatus OpenCloudFile();

		/**
		 * 选择另存位置
		 *
		 * @param mainFile		需要另存为的文件绝对路径
		 * @param isAssAssembly 是否为装配体文件
		 */
		ResultStatus SelectSaveAsPath(std::string mainFile, bool isAssAssembly);

		/**
		 * 保存文件
		 *
		 * @param saveFilePath  需要保存的文件绝对路径
		 * @param isAssAssembly 是否为装配体文件
		 */
		ResultStatus Save(std::string saveFilePath, bool isAssAssembly);

		/**
		 * 求解
		 *
		 * @param filePath  需要求解的文件绝对路径
		 */
		ResultStatus GetSolution(std::string filePath);

		/**
		 * 工业软件侧通知Iscdesk Client是否需要升级（非强制升级可用）
		 *
		 * @param bUpgrade  true表示需要升级，false表示不需要升级 
		 */
		ResultStatus UpgradeNotify(bool bUpgrade);

		/**
		 * 升级
		 *
		 */
		ResultStatus CheckNewVersions();

		/**
		 * 获取License文件请求函数
		 *
		 * @param fileName  license文件名称
		 * @param devInfo   设置指纹信息
		 */
		ResultStatus GetLicenseFileRequest(std::string fileName, std::string devInfo);

		/**
		 * 获取License字符串请求函数
		 *
		 * @param devInfo   设置指纹信息
		 */
		ResultStatus GetLicenseStringRequest(std::string devInfo);

		/**
		 * License激活结果通知函数
		 *
		 * @param verifyResult  验证结果
		 * @param errorMsg		错误信息
		 */
		ResultStatus LicenseVerifyNotify(bool verifyResult, std::string errorMsg);

		/**
		 * 软件试用到期通知函数
		 *
		 */
		virtual ResultStatus TrialExpirationNotify();

		/**
		 * 软件试用到期并获取license函数
		 *
		 * @param licType   请求license文件的类型
		 * @param devInfo   设备指纹信息
		 * @param fileName  license文件名称
		 */
		ResultStatus TrialExpirationAndGetLicense(LicenseType licType,
			std::string devInfo = "", std::string fileName = "");

	private:
		/* 获取当前应用的信息，包括port、appid等 */
		bool GetApplicationInfo();

		/* 连接Iscdesk Client */
		ResultStatus ConnectClient();

		/* 发送消息 */
		ResultStatus SendMsg(const std::string& msg);

		/* 接收回复的消息 */
		EnThreadLoop RecvReply(ThreadSleeper& sleeper, void* param);

		/* 回复消息处理 */
		int ReplyMessageHandle(void* data, uint32 dataLen, uint8 threadId);

		/* 回复置位 */
		bool ResetReceive();

		/* 检测iscdesk客户端是否存在 */
		EnThreadLoop CheckIscdeskClient(ThreadSleeper& sleeper, void* param);

		/* SDK事件转换 */
		RecallEvent EventTranslate(std::string event);

		/* SDK消息转换 */
		void MessageTranslate(RecallEvent evt, std::string& msg);

		/* 检查发送缓存，若缓存不足则进行扩容 */
		bool CheckSendBuffSize(int totalSize);

		/* 断线重连处理线程 */
		EnThreadLoop ReconnectIscdesk(ThreadSleeper& sleeper, void* param);

		/* 检查更新线程 */
		EnThreadLoop CheckNewVersionsFunc(ThreadSleeper& sleeper, void* param);

		/* 关闭socket */
		void CloseSocket();

		/* 快捷键处理 */
		void ShortcutKeyProc(std::string shortcutKeyName);

	private:
		SOCKET				m_clientSocket;				//tcp的socket
		HANDLE				m_closeEvent;				//TCP连接关闭监听事件
		ThreadInvker		m_recvThread;				//接收回复线程
		ThreadInvker		m_heartThread;				//心跳发送线程
		ThreadInvker		m_clientCheckThread;		//客户端检测线程
		ThreadInvker		m_reconnectThread;			//断线重连线程
		ThreadInvker		m_checkNewVersionThread;	//检查更新线程
		std::mutex			m_buffMutex;				//buff锁
		std::mutex			m_evtMutex;					//closeEvent的锁
		std::mutex			m_reconnectMutex;			//重连锁
		fd_set				m_fds;						//多路复用监听集合
		int					m_portId;					//服务端端口号
		int					m_sendBuffSize;				//发送消息buff长度
		char*				m_sendBuff;					//发送消息buff
		char*				m_replyBuff;				//接收回复数据的buff
		bool				m_isShutOut;				//是否已断开连接
		bool				m_verifyStatus;				//License验证状态
		std::string			m_licErrMsg;				//License错误原因
		std::string			m_licStatus;				//License获取的状态
		SdkMsgConvertor		m_msgConvertor;				//消息转换器
		ShortcutKeyOp*		m_shortcutKeyOp;			//快捷键操作对象
		ThreadQueue*		m_threadQueue;				//消息处理线程队列
	};
}

#endif