/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef SDK_IDL_HEAD_H
#define SDK_IDL_HEAD_H

#include <map>
#include <string>
#include "../sdkLibrary/Invoker.h"

namespace SDK
{
	enum ResultStatus 
	{
		STATUS_SUCCESS = 0,					//成功

		STATUS_FAILURE,						//普通错误
		ERROR_CLIENT_OFFLINE,				//客户端未登录
		ERROR_CLIENT_NOINSTALL,				//客户端未安装
		ERROR_INTERFACE_INVALID,			//接口不可用
		ERROR_SHM_READ_FAILED,				//共享内存读取失败

		//network
		ERROR_NETWORK_INITIALIZE_FAILED,	//网络初始化失败
		ERROR_NETWORK_ERROR,				//网络不可用
		ERROR_NETWORK_UNREACHBLE,			//网络不可达

		//license
		ERROR_LICENSE_EXPIRE,				//软件许可过期
		ERROR_LICENSE_NOEXIST,				//软件许可不存在

		ERROR_UNKNOWN						//未知错误
	};

	/* SDK回调事件 */
	enum RecallEvent
	{
		EVENT_HEARTBEAT_RESULT = 0,			//心跳回复事件
		EVENT_OPENCLOUDFILE_RESULT,			//打开云端文件事件
		EVENT_SAVE_RESULT,					//保存结果回复事件
		EVENT_UPDATE,						//升级事件
		EVENT_UPGRADENOTICE,				//升级信息查询通知事件
		EVENT_CLIENT_EXIT,					//iscdesk客户端关闭事件
		EVENT_LICENSE_VERIFY,				//license验证事件
		EVENT_SHORTCUT_SAVETOCLOUD,			//保存云端文件的快捷键事件
		EVENT_SHORTCUT_SAVEASTOCLOUD,		//另存为云端文件的快捷键事件
		EVENT_UPLOAD_START,					//文件上传云端开始事件
		EVENT_UPLOAD_END,					//文件上传云端结束事件
		EVENT_UNKONW						//未知事件
	};

	/* license类型 */
	enum LicenseType
	{
		LICENSE_TYPE_FILE = 0,				//文件license
		LICENSE_TYPE_STRING					//字符串license
	};

	class IscdeskSdkIdl
	{
	public:
		IscdeskSdkIdl() : m_eventFunc(NULL), m_openFileFunc(NULL), m_saveAsPathFunc(NULL),
			m_saveFunc(NULL), m_heartBeatFunc(NULL), m_closeAppFunc(NULL) {}

		/**
		* 初始化函数，必须首先调用，建立连接与初始化逻辑处理
		*
		*/
		virtual ResultStatus Init() = 0;

		/**
		* 退出函数，程序关闭之前调用。断开连接与释放资源
		*
		*/
		virtual ResultStatus ShutOut() = 0;

		/**
		* 心跳函数，工业软件与iscdesk保持心跳
		*
		*/
		virtual ResultStatus HeartBeat() = 0;

		/**
		* 打开云端文件
		*
		*/
		virtual ResultStatus OpenCloudFile() = 0;

		/**
		 * 选择另存位置
		 *
		 * @param mainFile		需要另存为的文件绝对路径
		 * @param isAssAssembly 是否为装配体文件
		 */
		virtual ResultStatus SelectSaveAsPath(std::string mainFile, bool isAssAssembly = false) = 0;

		/**
		 * 保存文件
		 *
		 * @param saveFilePath  需要保存的文件绝对路径
		 * @param isAssAssembly 是否为装配体文件
		 */
		virtual ResultStatus Save(std::string saveFilePath, bool isAssAssembly = false) = 0;

		/**
		 * 求解
		 *
		 * @param filePath  需要求解的文件绝对路径
		 */
		virtual ResultStatus GetSolution(std::string filePath) = 0;

		/**
		 * 工业软件侧根据通知Iscdesk Client是否需要升级（非强制升级可用）
		 *
		 * @param bUpgrade  true表示需要升级，false表示不需要升级
		 */
		virtual ResultStatus UpgradeNotify(bool bUpgrade) = 0;

		/**
		 * 升级
		 *
		 */
		virtual ResultStatus CheckNewVersions() = 0;

		/**
		 * 获取License文件请求函数
		 *
		 * @param fileName  license文件名称
		 * @param devInfo   设置指纹信息
		 */
		virtual ResultStatus GetLicenseFileRequest(std::string fileName, std::string devInfo) = 0;

		/**
		 * 获取License字符串请求函数
		 *
		 * @param devInfo   设置指纹信息
		 */
		virtual ResultStatus GetLicenseStringRequest(std::string devInfo) = 0;

		/**
		 * License激活结果通知函数
		 *
		 * @param verifyResult  验证结果
		 * @param errorMsg		错误信息
		 */
		virtual ResultStatus LicenseVerifyNotify(bool verifyResult, std::string errorMsg = "") = 0;

		/**
		 * 软件试用到期通知函数
		 *
		 */
		virtual ResultStatus TrialExpirationNotify() = 0;

		/**
		 * 软件试用到期并获取license函数
		 *
		 * @param licType   请求license文件的类型
		 * @param devInfo   设备指纹信息
		 * @param fileName  license文件名称
		 */
		virtual ResultStatus TrialExpirationAndGetLicense(LicenseType licType, 
			std::string devInfo = "", std::string fileName = "") = 0;

	public:
		/* 注册事件接收函数 */
		template<class T>
		DLL_API void RegisterEventRecall(T* obj, void(T::*func)(RecallEvent, std::string))
		{
			if (m_eventFunc != NULL)
			{
				delete m_eventFunc;
			}

			m_eventFunc = new InvokerT<void(RecallEvent, std::string), T>(obj, func);
		}

		/* 注册打开文件回调函数 */
		template<class T>
		DLL_API void RegisterOpenFile(T* obj, void(T::*func)(std::string))
		{
			if (m_openFileFunc != NULL)
			{
				delete m_openFileFunc;
			}

			m_openFileFunc = new InvokerT<void(std::string), T>(obj, func);
			m_invokerMaps[EVENT_OPENCLOUDFILE_RESULT] = m_openFileFunc;
		}

		/* 注册另存为函数 */
		template<class T>
		DLL_API void RegisterSelectSaveAsPathResult(T* obj, void(T::*func)(std::string))
		{
			if (m_saveAsPathFunc != NULL)
			{
				delete m_saveAsPathFunc;
			}

			m_saveAsPathFunc = new InvokerT<void(std::string), T>(obj, func);
		}

		/* 注册保存结果函数 */
		template<class T>
		DLL_API void RegisterSaveResult(T* obj, void(T::*func)(std::string))
		{
			if (m_saveFunc != NULL)
			{
				delete m_saveFunc;
			}

			m_saveFunc = new InvokerT<void(std::string), T>(obj, func);
			m_invokerMaps[EVENT_SAVE_RESULT] = m_saveFunc;
		}

		/* 注册接受心跳回调函数 */
		template<class T>
		DLL_API void RegisterHeartBeatResult(T* obj, void(T::*func)(std::string))
		{
			if (m_heartBeatFunc != NULL)
			{
				delete m_heartBeatFunc;
			}

			m_heartBeatFunc = new InvokerT<void(std::string), T>(obj, func);
			m_invokerMaps[EVENT_HEARTBEAT_RESULT] = m_heartBeatFunc;
		}

		/* 注册关闭函数 */
		template<class T>
		DLL_API void RegisterCloseApp(T* obj, void(T::*func)(std::string))
		{
			if (m_closeAppFunc != NULL)
			{
				delete m_closeAppFunc;
			}

			m_closeAppFunc = new InvokerT<void(std::string), T>(obj, func);
			m_invokerMaps[EVENT_CLIENT_EXIT] = m_closeAppFunc;
		}

	protected:
		Invoker<void(std::string)>*				m_openFileFunc;				//文件的回调函数
		Invoker<void(std::string)>*				m_saveAsPathFunc;			//另存为回调函数
		Invoker<void(std::string)>*				m_saveFunc;					//保存回调函数
		Invoker<void(std::string)>*				m_heartBeatFunc;			//心跳回调函数
		Invoker<void(std::string)>*				m_closeAppFunc;				//关闭应用回调函数

		Invoker<void(RecallEvent, std::string)>*			m_eventFunc;	//isv接收SDK事件后的回调函数
		std::map<RecallEvent, Invoker<void(std::string)>*>	m_invokerMaps;	//回调函数表
};
}

#endif