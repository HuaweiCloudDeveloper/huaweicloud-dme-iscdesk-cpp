/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef ISCDESK_SDK_DEFINES_H
#define ISCDESK_SDK_DEFINES_H

namespace SDK
{
#ifndef DLL_API
#ifdef _WIN32
#ifdef DLL_EXPORT
#define DLL_API _declspec(dllexport)
#elif DLL_IMPORT
#define DLL_API _declspec(dllimport)
#else
#define DLL_API 
#endif
#else
#define DLL_API 
#endif
#endif

const char* const	SAASCENTERCLIENT_SHM	= "Kooapp";
const char* const	LOOPBACK_IP				= "127.0.0.1";
const char* const	KOOAPP_REGISTRY_KEY_1	= "iscdesk";
const char* const	KOOAPP_REGISTRY_KEY_2	= "shell";
const char* const	KOOAPP_REGISTRY_KEY_3	= "open";
const char* const	KOOAPP_REGISTRY_KEY_4	= "command";
const char* const	KOOAPP_REGISTRY_KEY_5	= "";
const int			MAX_PATH_LEN			= 256;		//路径最大长度
const int			SDK_MSG_LEN				= 1024;		//sdk发送缓冲区长度 1K
const int			SDK_RECV_LEN			= 1048576;	//sdk接收缓冲区长度 1M
const int			SDK_MSG_HEAD_LEN		= 512;		//sdk消息头长度		512B
const int			SDK_LOG_LEN				= 1024;		//日志最大长度		1K

}

#endif
