/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef SDK_SHORTCUT_KEY_OPERATE_H
#define SDK_SHORTCUT_KEY_OPERATE_H

#include <windows.h>
#include <string>
#include <vector>

namespace SDK
{
	using HookFuncPtr = void(*)(std::string);

	class ShortcutKeyOp
	{
	public:
		ShortcutKeyOp();

		/* ע��hook��ؼ���������� */
		int	RegisterWindowsHook(HookFuncPtr func);

		/* ȥע��hook��� */
		void UnRegisterWindowsHook();

		/* ��ݼ����ý��� */
		void ShortcutKeyConfigParse(std::string msg);

	private:
		/* ��ݼ����� */
		std::vector<short> ShortcutKeyAarrange(std::string accelerator);

	private:
		HHOOK		m_hook;	//�����¼���ع���
	};
}

#endif
