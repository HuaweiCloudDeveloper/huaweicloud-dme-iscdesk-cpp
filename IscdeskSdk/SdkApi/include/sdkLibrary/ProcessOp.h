/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef SDK_PROCESS_OPERATE_H
#define SDK_PROCESS_OPERATE_H

#include <iostream>
#include <string>
#include <vector>
#include "../sdk/IscdeskSdkIdl.h"

namespace SDK
{
	extern DWORD g_processId;

	class ProcessOp
	{
	public:
		/* 获取Iscdesk客户端安装路径 */
		static int GetIscdeskClientInstallPath(std::string& path);

		/* 检查Iscdesk客户端是否已启动 */
		static int CheckIscdeskClientExecute();

		/* 将本进程的主窗口置为前景窗口 */
		static void SetForegroundMainWindow();

	private:
		/* EnumWindows回调函数，hwnd为发现的顶层窗口 */
		static BOOL CALLBACK EnumWindowsProc(HWND hWnd, LPARAM lParam);

	private:
		static std::vector<HWND> m_windowHandles; //当前进程的全部窗口
	};
}

#endif