/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef TIME_CLOCK_H
#define TIME_CLOCK_H

#include "../sdkLibrary/DataType.h"

namespace SDK
{
	class TimeClock
	{
	public:
		/* 获取当前cpu时钟ns数*/
		static uint64 GetCurrentTimeNs();

		/* 获取cpu时钟频率 */
		static uint64 GetPerformanceFreq();

		/* 获取cpu当前时钟数 */
		static uint64 GetPerformanceCounter();

	public:
		static uint64 m_freq;			//cpu时钟频率
		static uint64 m_initclk;		//程序刚启动时的时钟数
		static uint64 m_initns;			//程序启动时的时间ns
	};

	class Date
	{
	public:
		uint32	m_year;
		uint8	m_month;
		uint8	m_day;
		uint8	m_hour;
		uint8	m_min;
		uint8	m_sec;
		uint16	m_msec;
		uint16	m_usec;
		uint16	m_nsec;

	public:
		Date();

		explicit Date(uint64 ns);

		/* 以字符串的形式返回当前年-月-日 时:分:秒:毫秒 */
		std::string GetCurrentDate();

	private:
		/* 根据ns数计算当前日期 */
		void ComputeCurrentDate(uint64 ns);

		/* 获取当前时区 */
		int8 GetTimeZone();


	};
}

#endif