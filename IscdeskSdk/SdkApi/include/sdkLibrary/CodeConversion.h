/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef ANSI_UTF8_H
#define ANSI_UTF8_H

#include <string>
#include "DataType.h"

namespace SDK
{
	enum EnCodeType 
	{ 
		ANSI = 1, 
		UTF8,
		UTF8_BOM,
		UTF16_LE, 
		UTF16_BE,	 
	};

	class CodeConvertor
	{
	public:
		/* ��ԭ�ַ���ת��Ϊָ�������ʽ���ַ��� */
		static int CodeTypeConvert(const std::string& src, std::string& dst, EnCodeType dstType);

	private:
		static int CodeConvertToANSI(const std::string& src, std::string& dst, EnCodeType srcType);

		static int CodeConvertToUTF8(const std::string& src, std::string& dst, EnCodeType srcType);

		static int CodeConvertToUTF8BOM(const std::string& src, std::string& dst, EnCodeType srcType);

		static int CodeConvertToUTF16LE(const std::string& src, std::string& dst, EnCodeType srcType);

		static int CodeConvertToUTF16BE(const std::string& src, std::string& dst, EnCodeType srcType);
	};

	class CodeConvert
	{
	public:
		/* ����ַ��������ʽ */
		static EnCodeType CheckCodeType(const std::string& str, int ChineseNumber = 4);

		/* ANSI����תΪUTF8���� */
		static int ANSIToUTF8(const std::string& strAnsi, std::string& strUtf8);

		/* UTF8����תΪANSI���� */
		static int UTF8ToANSI(const std::string& strUtf8, std::string& strAnsi);

		/* ANSI����תΪUTF8BOM���� */
		static int ANSIToUTF8BOM(const std::string& strAnsi, std::string& strUtf8);

		/* UTF8BOM����תΪANSI���� */
		static int UTF8BOMToANSI(const std::string& strUTF8Bom, std::string& strAnsi);

		/* ANSI����תΪUTF16���룬isLE������ǰ�Ƿ�ΪUTF16 LE���� */
		static int ANSIToUTF16(const std::string& strAnsi, std::string& strUtf16, bool isLE);
		
		/* UTF16����תΪANSI���룬isLE������ǰ�Ƿ�ΪUTF16 LE���� */
		static int UTF16ToANSI(const std::string& strUTF16, std::string& strAnsi, bool isLE);

	private:
		/* ����ַ����Ƿ�����UTF8�ı������ */
		static bool CheckUTF8EncodingFormat(size_t size, uint8* data, int chineseNumber);
	};
}

#endif

