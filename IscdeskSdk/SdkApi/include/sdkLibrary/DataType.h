/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef DATA_TYPE_H
#define DATA_TYPE_H

namespace SDK
{
#ifndef NOUINT8
	typedef unsigned char uint8;
#endif

#ifndef NOINT8
	typedef char int8;
#endif

#ifndef NOUINT16
	typedef unsigned short uint16;
#endif

#ifndef NOINT16
	typedef short int16;
#endif

#ifndef NOUINT32
	typedef unsigned int uint32;
#endif

#ifndef NOINT32
	typedef int int32;
#endif

#ifndef NOUINT64
	typedef unsigned long long uint64;
#endif

#ifndef NOINT64
	typedef long long int64;
#endif
}

#endif
