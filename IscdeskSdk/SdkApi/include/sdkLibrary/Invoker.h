/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef INVOKER_H
#define INVOKER_H

namespace SDK
{
	template<class T> class Invoker;
	template<class U, class T = void> class InvokerT;

	/* 可变参数回调函数基类 */
	template <class R, typename... Types>
	class Invoker<R(Types...)>
	{
	public:
		virtual R Invoke(Types...) = 0;
		virtual ~Invoker() {}
	};

	/* 可变参数普通函数回调子类 */
	template <class R, typename... Types>
	class InvokerT<R(Types...)> :public Invoker<R(Types...)>
	{
	public:
		typedef R(*Function)(Types...);

	public:
		explicit InvokerT(Function func)
			:m_func(func) {}

		R Invoke(Types... args)
		{
			return (*m_func)(args...);
		}

	private:
		Function	m_func;
	};

	/* 可变参数类成员函数回调子类 */
	template <class R, class T, typename... Types>
	class InvokerT<R(Types...), T> :public Invoker<R(Types...)>
	{
	public:
		typedef R(T::*Function)(Types...);

	public:
		explicit InvokerT(T* obj, Function func)
			:m_obj(obj), m_func(func) {}

		R Invoke(Types... args)
		{
			return (m_obj->*m_func)(args...);
		}

	private:
		T*			m_obj;
		Function	m_func;
	};
}

#endif // ifndef
