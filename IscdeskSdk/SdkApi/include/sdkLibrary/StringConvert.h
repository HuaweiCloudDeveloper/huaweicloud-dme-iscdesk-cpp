/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef ISCDESK_SDK_STRING_CONVERT_H
#define ISCDESK_SDK_STRING_CONVERT_H

#include <string>

namespace SDK
{
	/* 数据转换接口类 */
	class StringConvert
	{
	public:
		/* 获取字符串中左右两个子字符串之间的内容 */
		static std::string ExtractStringBetween(const std::string& src, const std::string& left, 
			const std::string& right);

		/* 替换字符串中左右两个子字符串之间的内容 */
		static std::string ReplaceStringBetween(const std::string& src, const std::string& value, 
			const std::string& left, const std::string& right);

		/* 字符串转int函数 */
		static int	StringToInt(const std::string& str);

		/* 转化路径格式,"D:\\\\software"转换为"D:\\software" */
		static std::string PathDoubleSlashesToSingle(const std::string& str, bool bReverse = false);

		/* 转化路径格式,"D:\\\\software"转换为"D:/software" */
		static std::string PathDoubleSlashesToOpposite(const std::string& str, bool bReverse = false);

		/* 转化路径格式,"D:\\software"转换为"D:/software" */
		static std::string PathSingleSlashesToOpposite(const std::string& str, bool bReverse = false);

		/* 将bool类型转换为string类型 */
		static std::string BoolToStdString(bool src);

		/* 将字符串转换为大写 */
		static std::string StringToUpper(const std::string& str);

		/* 去掉路径最后一级 */
		static std::string RemoveLastLevel(const std::string& str);

		/* 规范化文件路径,使用delimiter作为路径分隔符 */
		static std::string CanonicalizePath(std::string path, std::string delimiter = "\\");

	private:
		/* 获取字符串src中左右字符串之间的字符串首尾位置 */
		static void GetStringPosition(const std::string& src, const std::string& left, 
			const std::string& right, size_t& startPos, size_t& endPos);

		/* 获取字符串下标最小的'\\'或'/' */
		static size_t FindSlashFromString(const std::string& str);
	};
}
#endif
