#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include "../sdkLibrary/DataType.h"
#include "RingBuffer.h"
#include "DataHandler.h"
#include "HandlerGroup.h"

namespace SDK
{
	/* 线程队列 */
	class ThreadQueue
	{
	public:
		~ThreadQueue();

		/* 初始化RingBuffer、线程池 */
		void Init(uint16 nodeNum, uint32 nodeSize, uint8 threadNum);

		/* 向ringbuff推送数据 */
		int PushBack(const void* data, uint32 len);

		/* 启动线程池 */
		void Start();

		/* 注册处理函数 */
		template<class T>
		void RegisterHandler(T* obj, int(T::*func)(void*, uint32, uint8))
		{
			if (m_handleGroup->m_ivker != NULL)
			{
				delete m_handleGroup->m_ivker;
			}

			m_handleGroup->m_ivker = new InvokerT<int(void*, uint32, uint8), T>(obj, func);
		}

		/* 注册处理函数 */
		void RegisterHandler(int(*func)(void*, uint32, uint8))
		{
			if (m_handleGroup->m_ivker != NULL)
			{
				delete m_handleGroup->m_ivker;
			}

			m_handleGroup->m_ivker = new InvokerT<int(void*, uint32, uint8)>(func);
		}

	private:
		RingBuffer*					m_ringBuffer;
		HandlerGroup*				m_handleGroup;
	};
}

#endif