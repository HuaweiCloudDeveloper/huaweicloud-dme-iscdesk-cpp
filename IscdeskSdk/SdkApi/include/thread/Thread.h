/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef SDK_THREAD_H
#define SDK_THREAD_H

#include <winsock2.h>   
#include "ThreadSema.h"
#include "ThreadSleeper.h"
#include "../sdkLibrary/Invoker.h"

namespace SDK
{
#define THREAD_CANCEL(thread)		(TerminateThread(thread, 0) == TRUE ? 0 : -1)
#define THREAD_JOIN(thread)			(WaitForSingleObject(thread, INFINITE) == WAIT_OBJECT_0 ? 0 : -1)
#define THREAD_CLOSE(thread)		(CloseHandle(thread) == TRUE ? 0 : -1)

	enum EnThreadLoop
	{
		THREAD_LOOP_EXIT = 0,
		THREAD_LOOP_CONTINUE = 1
	};

	class ThreadIdl
	{		 
		friend unsigned int __stdcall ThreadCall(void*);

	public:
		explicit ThreadIdl(const std::string& name = "");
		virtual ~ThreadIdl();

		int Start();

		int Stop(int ms = 5000);

		bool IsStoped();

		virtual int Run() = 0;

	protected:
		bool			m_bShouldExit;		//是否要退出线程
		bool			m_bStoped;			//线程当前是否停止
		std::string		m_threadName;		//线程名称
		ThreadSleeper	m_sleeper;			//线程睡眠器
		HANDLE			m_threadHandle;		//线程句柄
	};

	class ThreadInvker :public ThreadIdl
	{
	public:
		explicit ThreadInvker(const std::string& name = "");
		ThreadInvker(const ThreadInvker&) = delete;
		void operator=(ThreadInvker&) = delete;

		~ThreadInvker();

		int Run();

	public:
		/* 注册线程回调函数 */
		template<class T>
		bool RegisterThreadFunc(T* obj, EnThreadLoop(T::*func)(ThreadSleeper&, void*), void* param)
		{
			if (m_bStoped)
			{
				if (m_invoker != NULL)
				{
					delete m_invoker;
					m_invoker = NULL;
				}

				m_param = param;
				m_invoker = new InvokerT<EnThreadLoop(ThreadSleeper&, void*), T>(obj, func);
				return true;
			}

			return false;
		}

		/* 注册线程回调函数 */
		bool RegisterThreadFunc(EnThreadLoop(*func)(ThreadSleeper&, void*), void* param)
		{
			if (m_bStoped)
			{
				if (m_invoker != NULL)
				{
					delete m_invoker;
					m_invoker = NULL;
				}

				m_param = param;
				m_invoker = new InvokerT<EnThreadLoop(ThreadSleeper&, void*)>(func);

				return true;
			}

			return false;
		}

	private:
		Invoker<EnThreadLoop(ThreadSleeper&, void*)>*	m_invoker;	//注册到线程中的回调函数
		void*											m_param;	//回调函数的参数
	};
}

#endif