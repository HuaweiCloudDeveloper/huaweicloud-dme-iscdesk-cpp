/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef THREAD_SLEEP_H
#define THREAD_SLEEP_H

#include "ThreadSema.h"

namespace SDK
{
	enum SleepIRQ
	{
		IRQ_THREADKILL,		/* 线程要退出 */
		IRQ_USERNOTIFY,		/* 业务通知 */
		IRQ_TIMEOUT,		/* 等待超时 */
		IRQ_ELSE,			/* 不可知中断 */
		IRQ_NULL,			/* 空 */
	};

	/* 线程睡眠器 */
	class ThreadSleeper
	{
	public:
		ThreadSleeper() :m_irq(IRQ_NULL), m_sleeping(false) {}

		/* 睡眠器重置 */
		void Reset();

		/* 一直睡眠，等待通知 */
		SleepIRQ Sleep();

		/* 睡眠ms时间 */
		SleepIRQ Sleep(int ms);

		/* 中断睡眠 */
		void Interrupt(SleepIRQ irq = IRQ_USERNOTIFY);

		/* 返回是否正在睡眠 */
		bool IsSleep();

		/* 获取当前中断状态 */
		SleepIRQ GetIrq();
		
	private:
		SleepIRQ ThreadSleep(bool btrywait, int ms);

	private:
		ThreadSema	m_timeSema;
		SleepIRQ	m_irq;
		bool		m_sleeping;
	};
}


#endif