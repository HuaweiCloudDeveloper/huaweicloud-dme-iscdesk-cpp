#ifndef HANDLER_GROUP_H
#define HANDLER_GROUP_H
#include <vector>
#include "../sdkLibrary/DataType.h"
#include "RingBuffer.h"
#include "DataHandler.h"

namespace SDK
{
	class HandlerGroup
	{
		friend class DataHandler;
		friend class ThreadQueue;
	public:
		HandlerGroup(RingBuffer* ringBuffer, uint8 threadNum);
		~HandlerGroup();

		/* 注册数据处理者 */
		void RegisterDataHandler(DataHandler* dataHandler);

		/* 启动处理组 */
		void Start();

		/* 停止处理组 */
		void Stop();

		/* 判断处理组是否来得及处理 */
		bool IsCanWrite(uint32 wseq);

		/* 数据通知 */
		void DataNotify();

	private:
		RingBuffer*							m_ringBuffer;
		std::vector<DataHandler*>			m_handlers;
		std::vector<WaitStrategy*>			m_waitStrategys;
		Invoker<int(void*, uint32, uint8)>*	m_ivker;
		uint32								m_readSeq;
		std::mutex							m_mutex;

	};
}

#endif