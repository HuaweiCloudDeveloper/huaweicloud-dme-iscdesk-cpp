#ifndef RING_BUFFER_H
#define RING_BUFFER_H

#include <mutex>
#include "../sdkLibrary/DataType.h"

namespace SDK
{
	struct RBNode
	{
		uint8			m_flag;		//提交标记，0表示未提交，1表示已提交
		uint8			m_fill[3];	//填充8字节对齐
		int				m_len;		//节点长度
		char			m_data[0];	//节点实际数据

		RBNode(const RBNode&) = delete;
		RBNode& operator=(const RBNode&) = delete;
	};

	class HandlerGroup;
	class RingBuffer
	{
		friend class DataHandler;
	public:
		RingBuffer(uint16 num, uint32 size);

		~RingBuffer();

		/* 注册处理组 */
		void RegisterHandlerGroup(HandlerGroup* handlerGroup);

		/* 尝试获取下一个写序号 */
		int TryNext(uint32& wSeq);

		/* 提交数据使用完毕 */
		void Commit(uint32 seq, int dataLen);

		/* 返回序号对应节点数据是否有效 */
		bool IsAvailable(uint32 seq);

		/* 返回节点数量 */
		uint16 NodeNum();

		/* 返回节点长度 */
		uint32 NodeSize();

		/* 根据序号返回RBNode节点 */
		RBNode* GetRBNode(uint32 seq);

	private:
		char*			m_buff;			//缓存地址
		uint16			m_nodeNum;		//节点个数
		uint32			m_nodeSize;		//节点大小
		uint32			m_writeSeq;		//写序号
		std::mutex		m_mutex;		//线程锁
		HandlerGroup*	m_handlerGroup;	//处理组
	};
}

#endif
