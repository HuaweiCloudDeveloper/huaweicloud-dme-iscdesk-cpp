#ifndef DATA_HANDLE_H
#define DATA_HANDLE_H

#include "../sdkLibrary/Invoker.h"
#include "Thread.h"
#include "ThreadSleeper.h"
#include "RingBuffer.h"

namespace SDK
{
	class DataHandler;
	class WaitStrategy
	{
	public:
		/* 等待数据 */
		bool WaitData(DataHandler* dataHandler);

		/* 启动 */
		void Start();

		/* 停止 */
		void Stop();

		/* 中断 */
		void Interrupt();

	private:
		ThreadSleeper m_sleeper;
	};

	class HandlerGroup;
	class DataHandler
	{
		friend class WaitStrategy;
		friend class HandlerGroup;
	public:
		DataHandler(HandlerGroup* handlerGroup, RingBuffer* ringBuffer, 
			WaitStrategy* waitStrategy, uint8 threadId = 0);
		~DataHandler();

		/* 启动数据处理线程 */
		void Start();

		/* 停止数据处理线程 */
		void Stop();

		/* 线程是否正忙 */
		bool IsBusy();

	private:
		/* 数据处理线程 */
		EnThreadLoop DataProcess(ThreadSleeper& sleeper, void* param);

		/* 返回等待条件是否满足 */
		bool WaitCondition();

	private:
		HandlerGroup*	m_handlerGroup;
		RingBuffer*		m_ringBuffer;
		WaitStrategy*	m_waitStrategy;
		ThreadInvker	m_handleThread;
		ThreadSleeper	m_sleeper;
		uint32			m_readSeq;
		int				m_threadId;
		bool			m_isOccupy;
	};
}

#endif