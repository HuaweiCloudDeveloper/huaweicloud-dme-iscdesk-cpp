/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef THREAD_MUTEX_H
#define THREAD_MUTEX_H

#include <winsock2.h>   

namespace SDK
{
#ifdef _WIN32

	/* 信号量相关 */
	typedef HANDLE THREAD_SEMA_HANDLE;//定义信号量
#define THREAD_SEMA_INIT(sem)			(((sem) = CreateSemaphore(NULL, 0,0x7FFFFFFF, NULL)) != 0 ? 0 : -1)
#define THREAD_SEMA_DESTORY(sem)		(CloseHandle(sem) == TRUE ? 0 : -1)
#define THREAD_SEMA_WAIT(sem)			(WaitForSingleObject(sem, INFINITE) == WAIT_OBJECT_0 ? 0 : -1)
#define THREAD_SEMA_TRYWAIT(sem, t)		(WaitForSingleObject(sem, t) == WAIT_OBJECT_0 ? 0 : -1)
#define THREAD_SEMA_POST(sem)			(ReleaseSemaphore(sem, 1, NULL) == TRUE ? 0 : -1)

#endif

	class ThreadSema
	{
	public:
		ThreadSema();
		~ThreadSema();

		int	Post();
		int	Wait();
		int	TryWait(int ms);

		void Reset();

	private:
		THREAD_SEMA_HANDLE m_sema;
	};
}

#endif