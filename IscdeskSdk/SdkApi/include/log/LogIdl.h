/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef LOG_IDL_H
#define LOG_IDL_H

#include <string>
#include "../sdkLibrary/TimeClock.h"

namespace SDK
{
	/* 日志等级 */
	enum LogLevel
	{
		SDKLOG_DEBUG = 0,		//日志等级：调试
		SDKLOG_INFO,			//日志等级：信息
		SDKLOG_WARN,			//日志等级：警告
		SDKLOG_ERROR			//日志等级：错误
	};

	class LogIdl
	{
	public:
		/* 写日志 */
		virtual void WriteLog(LogLevel logLevl, const std::string& logContent) = 0;

		std::string LogLevelConvert(LogLevel logLevel)
		{
			std::string ret = "[";

			switch (logLevel)
			{
			case SDKLOG_INFO:
				ret += "Info";
				break;
			case SDKLOG_DEBUG:
				ret += "Debug";
				break;
			case SDKLOG_WARN:
				ret += "Warn";
				break;
			case SDKLOG_ERROR:
				ret += "Error";
				break;
			default:
				break;
			}

			ret += "]";

			return ret;
		}

	public:
		LogLevel	m_logShowLevel;	//日志显示等级
	};
}

#endif
