/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef LOG_WRITECONSOLE_H
#define LOG_WRITECONSOLE_H

#include <string>

namespace SDK
{
	class LogWriteConsole
	{
		friend class LogImpl;
	private:
		void WriteToConsole(const std::string& logContent);
	};
}

#endif