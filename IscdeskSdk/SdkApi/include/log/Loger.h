/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef LOG_LOGER__H
#define LOG_LOGER__H

#include "../../include/sdkLibrary/Defines.h"
#include "LogImpl.h"

namespace SDK
{
	extern LogImpl g_loger;

	template<typename T>
	struct item_return {
		using type = T && ;
	};

	template<typename T>
	inline typename item_return<T>::type convert(T&& arg) {
		return static_cast<T&&>(arg);
	}

	template <class... Args>
	void PrintLog(LogLevel logLevl, const char* format, Args&&... args)
	{
		const size_t logBuffSize = SDK_LOG_LEN + SDK_MSG_HEAD_LEN;//日志缓冲区长度最大为日志限定长度+消息头长度
		char buff[logBuffSize];

		if (_snprintf_s(buff, logBuffSize, format, convert(std::forward<Args>(args))...) < 0)
		{
			return;
		}

		g_loger.WriteLog(logLevl, buff);
	}

#define GLogInfo(format, ...)		PrintLog(SDKLOG_INFO, format, __VA_ARGS__)
#define GLogDebug(format, ...)		PrintLog(SDKLOG_DEBUG, format, __VA_ARGS__)
#define GLogWarning(format, ...)	PrintLog(SDKLOG_WARN, format, __VA_ARGS__)
#define GLogError(format, ...)		PrintLog(SDKLOG_ERROR, format, __VA_ARGS__)
#define GLogCheckSize(logStr)		g_loger.CheckLogSize(logStr)
}
#endif