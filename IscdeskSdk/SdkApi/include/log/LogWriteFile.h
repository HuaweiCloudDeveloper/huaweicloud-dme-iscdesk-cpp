/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef LOG_WRITEFILE_H
#define LOG_WRITEFILE_H

#include <string>
#include <mutex>
#include "../sdkLibrary/TimeClock.h"

namespace SDK
{
	class LogWriteFile
	{
		friend class LogImpl;
	public:
		/* 待实现logDir = "./log" */
		explicit LogWriteFile(std::string logName = "sdk.log", std::string logDir = "./");
		LogWriteFile(const LogWriteFile&) = delete;
		void operator=(LogWriteFile&) = delete;

		~LogWriteFile();

		/* 设置日志切片大小 */
		void SetCutSize(int cutSize);

	private:
		/* 日志写文件 */
		void WriteFile(const std::string& logContent);

		/* 日志文件清空 */
		void Clear();

	private:
		std::string		m_logName;		//日志文件名
		std::string		m_logDir;		//日志文件夹路径
		FILE*			m_fp;			//文件指针
		bool			m_cutBySize;	//当前是否根据大小清空
		int				m_maxSize;		//日志文件最大size
		int				m_currentSize;	//日志文件当前size
		std::mutex		m_mutex;		//日志写文件锁
	};
}

#endif