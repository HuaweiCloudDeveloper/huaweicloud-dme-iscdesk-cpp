/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2024. All rights reserved.
 */

#ifndef LOG_IMPL_H
#define LOG_IMPL_H

#include "LogIdl.h"
#include "LogWriteFile.h"
#include "LogWriteConsole.h"

namespace SDK
{
	class LogImpl: public LogIdl
	{
	public:
		LogImpl();
		LogImpl(const LogImpl&) = delete;
		void operator=(LogImpl&) = delete;

		~LogImpl();

		/* 写日志 */
		void WriteLog(LogLevel logLevel, const std::string& logContent);

		/* 设置日志显示等级 */
		void SetLogShowLevel(LogLevel logShowLevel);

		/* 日志长度检查，超过限定长度则截断 */
		void CheckLogSize(std::string& logStr);

	private:
		LogWriteFile*		m_fileWriter;		//日志文件写者
		LogWriteConsole*	m_consoleWriter;	//日志控制台写者

	};
}

#endif
